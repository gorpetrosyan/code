import { IonApp, IonRouterOutlet } from "@ionic/vue";
import { defineComponent } from "vue";

export default defineComponent({
  name: "TheZportyApp",
  components: {
    IonApp,
    IonRouterOutlet,
  },
  setup() {
    return () => (
      <IonApp>
        <IonRouterOutlet></IonRouterOutlet>
      </IonApp>
    );
  },
});
