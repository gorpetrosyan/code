import moment from "moment";
import * as dayjs from "dayjs";

/**
 * date format
 * @param date
 * @param format
 */
export function globalDateFormat(date: string, format: string): string {
  return moment(date).format(format);
}

/**
 * date from now
 * @param date
 */
export function globalDateFromNow(date: string): string {
  return moment(date).fromNow();
}
