export default function (arr: Array<object>, ident: number) {
  return arr.find((item: any) => {
    if (item.id === ident) {
      return item;
    }
  });
}
