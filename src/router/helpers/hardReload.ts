import { LoadingInstance } from "@/common/services/notifications/loader/loaderService";
import router from "..";

const Loader = new LoadingInstance();
const RELOADING = "hardReload";
const TIMEOUT = 1000;
const LOADER = { message: "Loading...", duration: TIMEOUT, cssClass: "hard-reloader", backdropDismiss: false };

function hardReload(path = "") {
  let toPathName;
  let toPathParams;
  if (path) {
    toPathName = path.toString();
    toPathParams = {};
  } else {
    toPathName = router.currentRoute.value.name?.toString();
    toPathParams = router.currentRoute.value.params || {};
  }

  sessionStorage[RELOADING] = true;
  Loader.showLoader(LOADER);
  router.replace("/");
  router.replace({
    name: toPathName,
    params: toPathParams,
  });
  setTimeout(() => {
    window.location.reload();
  }, TIMEOUT);
}

router.beforeEach(() => {
  if (sessionStorage[RELOADING]) {
    Loader.showLoader(LOADER);
  }
});
router.afterEach(() => {
  if (sessionStorage[RELOADING]) {
    sessionStorage.removeItem(RELOADING);
    Loader.hideLoader();
  }
});

export { hardReload };
