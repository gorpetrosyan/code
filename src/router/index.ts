import { RouteRecordRaw } from "vue-router";
import { createRouter, createWebHistory } from "@ionic/vue-router";
import { modules } from "@/config/modules";
import moduleNames from "@/config/modules/moduleTypes";

import { authGuard } from "./checks";

const routes: Array<RouteRecordRaw> = [
  // Auth Module
  {
    path: "/",
    redirect: "/auth",
  },

  {
    path: "/auth",
    component: () => modules[moduleNames.AUTHENTICATION].path,
    meta: {
      hideForAuth: true,
    },
    children: modules[moduleNames.AUTHENTICATION].routes,
  },

  {
    path: "/dashboard",
    component: () => modules[moduleNames.DASHBOARD].path,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "playground",
        component: () => modules[moduleNames.SOCIAL_MEDIA].path,
        children: modules[moduleNames.SOCIAL_MEDIA].routes,
      },
      {
        path: "explore",
        component: () => modules[moduleNames.EXPLORE].path,
        children: modules[moduleNames.EXPLORE].routes,
      },
      {
        path: "events",
        component: () => modules[moduleNames.EVENTS].path,
        children: modules[moduleNames.EVENTS].routes,
      },
      {
        path: "notifications",
        component: () => modules[moduleNames.NOTIFICATIONS].path,
        children: modules[moduleNames.NOTIFICATIONS].routes,
      },
      {
        path: "messages",
        component: () => modules[moduleNames.MESSAGES].path,
        children: modules[moduleNames.MESSAGES].routes,
      },

      {
        path: "profiles",
        component: () => modules[moduleNames.PROFILES].path,
        children: modules[moduleNames.PROFILES].routes,
      },
      {
        path: "settings",
        component: () => modules[moduleNames.SETTINGS].path,
        children: modules[moduleNames.SETTINGS].routes,
      },
    ],
  },

  {
    path: "/public",
    component: () => modules[moduleNames.PUBLIC].path,
    children: [
      {
        path: "info",
        component: () => modules[moduleNames.INFO].path,
        children: modules[moduleNames.INFO].routes,
      },
    ],
  },

  // 404
  {
    path: "/:pathMatch(.*)*",
    component: () => import("@/common/components/route-error/TheRouterError"),
  },
];

// Create Router
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

// Router Hooks
router.beforeEach((to, from, next) => {
  authGuard(to, from, next);
});

export default router;
