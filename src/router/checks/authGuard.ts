import { useLocalStorage } from "@/common/services/storage/localStorageService";

const AUTH_KEY = "user_id";

function authGuard(to: any, from: any, next: any): any {
  const token = useLocalStorage(AUTH_KEY).value;
  if (to.matched.some((record: any) => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!token) {
      next({ name: "Login" });
    } else {
      next(); // go to wherever I'm going
    }
  } else if (to.matched.some((record: any) => record.meta.hideForAuth)) {
    if (token) {
      next({ name: "PlaygroundLanding" });
    } else {
      next();
    }
  } else {
    next(); // does not require auth, make sure to always call next()!
  }
}

export { authGuard };
