import { profileApi } from "@/common/services/api";
import { AllActionTypes } from "@/store/actionTypes";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";

/**
 * @param store
 * @param groupId
 */
export default async function (store: any, groupId: number) {
  await profileApi
    .getClubSports(groupId)
    .then((data: any) => {
      if (data) {
        store.dispatch(AllActionTypes.GET_SPORTS, data);
      } else {
        errorToaster("Something went wrong");
      }
    })
    .catch((e: any) => {
      errorToaster(e.message);
    });
}
