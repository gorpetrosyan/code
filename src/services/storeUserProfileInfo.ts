import { profileApi } from "@/common/services/api";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";
import { AllActionTypes } from "@/store/actionTypes";

/**
 * @param store
 */
export default async function (store: any) {
  await profileApi
    .me()
    .then((data: any) => {
      if (data.profiles) {
        store.dispatch(AllActionTypes.GET_PROFILE, data.profiles);
      } else {
        errorToaster("Something went wrong");
      }
    })
    .catch((e: any) => {
      errorToaster(e.message);
    });
}
