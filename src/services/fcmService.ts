import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
  Capacitor
} from '@capacitor/core';
import {useRouter} from 'vue-router';
import { useStore } from "@/use/useStore";
import { AllActionTypes } from "@/store/actionTypes";
import {computed} from "vue";
import storeFcmToken from "@/common/services/notifications/push/notificationActions/storeFcmToken";
import {toastController} from "@ionic/vue";
import moment from "moment";
import {close, informationCircleOutline} from "ionicons/icons";
import markAsRead from "@/common/services/notifications/push/notificationActions/markAsRead";

const {PushNotifications} = Plugins;

export class FcmService {

  constructor(private router = useRouter()) {
  }

  public initPush() {
    if (Capacitor.platform !== 'web') {
      this.registerPush();
    }
  }

  private registerPush() {
    PushNotifications.requestPermission().then((permission) => {
      if (permission.granted) {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // No permission for push granted
      }
    })
    PushNotifications.addListener(
      'registration',
      (token: any) => {
        prompt(token.value)
        storeFcmToken(token.value);
      }
    );

    PushNotifications.addListener('registrationError', (error: any) => {
      console.log('Error: ' + JSON.stringify(error));
    });

    PushNotifications.addListener(
      'pushNotificationReceived',
      async (notification: any) => {
        const modalT = await toastController.create({
          message: notification.text + '<br>' + moment(notification.notificationTime).format('HH:MM:SS'),
          color: 'light',
          buttons: [
            {
              side: 'start',
              icon: informationCircleOutline,
              handler: () => {
                this.router.push({
                  name: notification.action.details.name,
                  params: notification.action.details.params,
                })
                markAsRead(notification.id)
              }
            }, {
              icon: close,
              role: 'cancel',
            }
          ],
          position: 'top',
          header: notification.title,
          duration: 5000,
        });
        return modalT.present();
      }
    );

    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      async (notification: any) => {
        await this.router.push({
          name: notification.action.details.name,
          params: notification.action.details.params,
        })
        markAsRead(notification.id)
      }
    );
  }
}

