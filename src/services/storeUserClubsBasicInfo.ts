import { profileApi } from "@/common/services/api";
import { AllActionTypes } from "@/store/actionTypes";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";

/**
 * @param store
 * @param profileId
 */
export default async function (store: any, profileId: number) {
  await profileApi
    .getBasicClubInfo(profileId)
    .then((data: any) => {
      if (data) {
        store.dispatch(AllActionTypes.GET_CLUB_STATES, data);
      } else {
        errorToaster("Something went wrong");
      }
    })
    .catch((e: any) => {
      errorToaster(e.message);
    });
}
