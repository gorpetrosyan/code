import { useApi } from "@/common/services/api/api";
import { chatTokenFetchUrl } from "@/common/services/api/endpoints";
import { useLocalStorage } from "@/common/services/storage/localStorageService";

const actionsList = {
  async fetchChatTokenAction(context: any): Promise<any> {
    const headerParams = {
      profileId: useLocalStorage("profile_id").value,
      profileType: "free",
    };
    const { get } = useApi(chatTokenFetchUrl);
    if (!context.getters["chat/gettersList"].chatTokenStateGetter) {
      return get()
        .then((res) => {
          console.log(res, "req");
          context.commit("chat/mutationsList/fetchChatTokenMutation", res.token);
        })
        .catch((e) => {
          console.log(e, "fetchChatTokenAction");
        });
    } else {
      return true;
    }
  },
};

export default {
  actionsList,
};
