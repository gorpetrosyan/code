const mutationsList = {
  fetchChatTokenMutation(state: any, token: string){
    state.chat.token = token
  }
}

export default {
  mutationsList
}
