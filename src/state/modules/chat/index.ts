 import actions from './actions';
 import getters from './getters';
 import mutations from './mutations';
//

 // store.ts
 import { createStore } from 'vuex'

 // define your typings for the store state
 export interface ChatInterface {
   token: string;
 }

 // define injection key

 export const store = createStore<ChatInterface>({
   state: {
     token: ''
   }
 })




export default {
  namespaced: true,
  store,
  actions,
  getters,
  mutations,
};
