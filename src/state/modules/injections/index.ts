import { InjectionKey } from "vue";
import { Store } from "vuex";
import { ChatInterface } from "@/state/modules/chat";

export const chatKey: InjectionKey<Store<ChatInterface>> = Symbol();
