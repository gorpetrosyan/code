import Vue from "vue";
import Vuex from "vuex";

import home from "./home.module";
import auth from "../modules/auth/state/auth.module";
import article from "./article.module";
import profile from "./profile.module";
import chat from "./modules/chat";
// Vue.use(Vuex);

// export default new Vuex.Store({
//   modules: {
//     home,
//     auth,
//     article,
//     profile,
//     chat,
//   },
// });
