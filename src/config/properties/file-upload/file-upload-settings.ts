/**
 * Media limitation during upload
 */
export const mediaLimitMax = 10;

/**
 * Uploaded photo quality
 */
export const photoQuality = 70;

/**
 * Uploaded video quality
 */
export const videoQuality = 50;

/**
 * Uploaded video duration
 */
export const videoDuration = 30;

/**
 * Uploaded photo size
 */
export const photoSize = 10485760;

/**
 * Uploaded video size
 */
export const videoSize = 52428800;

/**
 * compression size
 */
export const compressorQuality = 0.8;

/**
 *  media file type and size
 **/
export const mediaFileTypesAndSize: { [key: string]: any } = {
  "video/mp4": {
    size: videoSize,
    type: "video",
  },
  "video/webm": {
    size: videoSize,
    type: "video",
  },
  "video/ogg": {
    size: videoSize,
    type: "video",
  },
  "image/png": {
    size: photoSize,
    type: "image",
  },
  "image/jpeg": {
    size: photoSize,
    type: "image",
  },
  "image/bmp": {
    size: photoSize,
    type: "image",
  },
  "application/pdf": {
    size: photoSize,
    type: "pdf",
  },
};
/**
 * media types
 **/
export const mediaTypes = {
  VIDEO: "video",
  IMAGE: "image",
  PDF: "pdf",
};

/**
 * media types which get component
 */
export const componentMediaPropsTypes = {
  PHOTO: "photo",
  VIDEO: "video",
  IMAGE: "image",
  MEDIA: "media",
  PDF: "pdf",
};

/**
 * storage type
 */
export const storageType = {
  PUBLIC: "public",
  PRIVATE: "private",
};
