/**
 * must contain @ symbol, valid email symbols
 */
export const emailRegEx = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
/**
 * must contains at least 3 symbols
 */
export const textRegEx = /^.{3,}$/;

/**
 * must contain alphabetical symbols , min 3 symbols,
 */
export const nameRegEx = /^[a-zA-Z]{3,}$/;

/**
 * (?=.*\d)          // should contain at least one digit
 * (?=.*[a-z])       // should contain at least one lower case
 * (?=.*[A-Z])       // should contain at least one upper case
 * [a-zA-Z0-9]{8,25}   // should contain at least 8 from the mentioned characters to 25
 */
export const passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,25}$/;

/**
 *  must contain at least from 3-16 chars including A-z 0-9 and _ symbol
 */
export const usernameRegEx = /^[A-z0-9_-]{3,16}$/;

/**
 *  must contain www followed by domain name
 */
export const websiteRegEx = /^(www.)[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$/;
