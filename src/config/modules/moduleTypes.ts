enum moduleNames {
  AUTHENTICATION = "Authentication",
  DASHBOARD = "Dashboard",
  EVENTS = "Events",
  EXPLORE = "Explore",
  MESSAGES = "Messages",
  NOTIFICATIONS = "Notifications",
  PROFILES = "Profiles",
  SOCIAL_MEDIA = "SocialMedia",
  SETTINGS = "Settings",
  PUBLIC = "Public",
  INFO = "Info",
}

export default moduleNames;
