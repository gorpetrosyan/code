import { homeOutline, chatboxEllipsesOutline, notificationsOutline, searchOutline, addOutline } from "ionicons/icons";

export const navOptions = () => {
  return [
    { id: 1, tab: "home", page: "PlaygroundLanding", icon: homeOutline, label: "Home" },
    { id: 2, tab: "explore", page: "ExploreLanding", icon: searchOutline, label: "Explore" },
    { id: 3, tab: "create", page: "CreateBasicEvent", icon: addOutline, label: "Create" },
    {
      id: 4,
      tab: "notifications",
      page: "NoficationsList",
      icon: notificationsOutline,
      label: "Notifications",
    },
    { id: 5, tab: "messages", page: "ChatFrame", icon: chatboxEllipsesOutline, label: "Messages" },
  ];
};
