import { AuthModuleInfo } from "@/modules/auth/AuthModuleInfo";
import { DashboardModuleInfo } from "@/modules/dashboard/DashboardModuleInfo";
import { EventsModuleInfo } from "@/modules/dashboard/events/EventsModuleInfo";
import { ExploreModuleInfo } from "@/modules/dashboard/explore/ExploreModuleInfo";
import { MessagesModuleInfo } from "@/modules/dashboard/messages/MessagesModuleInfo";
import { NotificationsModuleInfo } from "@/modules/dashboard/notifications/NotificationsModuleInfo";
import { ProfilesModuleInfo } from "@/modules/dashboard/profiles/ProfilesModuleInfo";
import { SocialMediaModuleInfo } from "@/modules/dashboard/social-media/SocialMediaModuleInfo";
import { SettingsModuleInfo } from "@/modules/dashboard/settings/SettingsModuleInfo";
import { PublicModuleInfo } from "@/modules/public/PublicModuleInfo";
import { InfoModuleInfo } from "@/modules/public/info/InfoModuleInfo";

const modules = {
  ...AuthModuleInfo,
  ...DashboardModuleInfo,
  ...EventsModuleInfo,
  ...ExploreModuleInfo,
  ...MessagesModuleInfo,
  ...NotificationsModuleInfo,
  ...ProfilesModuleInfo,
  ...SocialMediaModuleInfo,
  ...SettingsModuleInfo,
  ...PublicModuleInfo,
  ...InfoModuleInfo,
};

export { modules };
