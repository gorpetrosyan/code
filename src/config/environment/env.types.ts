enum envs {
  DEV = "development",
  BETA = "beta",
  PROD = "production",
}

export default envs;
