import Env from "./env.model";
import MODES from "./env.types";

const env: Env = {
  MODE: MODES.BETA,
  API_URL: "https://beta-api.zporty.com/api/",
  AUTH_URL: "https://beta-accounts.zporty.com/api/",
  CHAT_URL: "https://beta-chat.zporty.com/",
  APP_URL: "https://beta-app.zporty.com/",
  APP_PAGINATION: 10,
};

export default env;
