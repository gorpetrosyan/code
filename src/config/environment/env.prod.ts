import Env from "./env.model";
import MODES from "./env.types";

const env: Env = {
  MODE: MODES.PROD,
  API_URL: "https://api.zporty.com/api/",
  AUTH_URL: "https://accounts.zporty.com/api/",
  CHAT_URL: "https://chat.zporty.com/",
  APP_URL: "https://app.zporty.com/",
  APP_PAGINATION: 10,
};

export default env;
