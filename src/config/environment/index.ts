import DEV from "./env.dev";
import BETA from "./env.beta";
import PROD from "./env.prod";
import Env from "./env.model";
import envs from "./env.types";

let ENVIRONMENT: Env;
if (process.env.NODE_ENV == envs.PROD) {
  ENVIRONMENT = PROD;
} else if (process.env.NODE_ENV == envs.BETA) {
  ENVIRONMENT = BETA;
} else {
  ENVIRONMENT = DEV;
}

export default { ...ENVIRONMENT };
