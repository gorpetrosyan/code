interface Env {
  MODE: string;
  API_URL: string;
  AUTH_URL: string;
  CHAT_URL: string;
  APP_URL: string;
  APP_PAGINATION: number;
}

export default Env;
