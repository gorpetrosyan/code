import Env from "./env.model";
import MODES from "./env.types";

const env: Env = {
  MODE: MODES.DEV,
  AUTH_URL: "https://dev-accounts.zporty.com/api/",
  API_URL: "https://dev-api.zporty.com/api/",
  CHAT_URL: "https://dev-chat.zporty.com/",
  APP_URL: "https://beta-app.zporty.com/",
  APP_PAGINATION: 2,
};

export default env;
