import moduleNames from "@/config/modules/moduleTypes";

const PublicModuleInfo = {
  [moduleNames.PUBLIC]: {
    path: import("@/modules/public/PublicModule"),
    routes: [],
    state: {},
  },
};

export { PublicModuleInfo };
