import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const InfoModuleInfo = {
  [moduleNames.INFO]: {
    path: import("@/modules/public/info/InfoModule"),
    routes,
    state: {},
  },
};

export { InfoModuleInfo };
