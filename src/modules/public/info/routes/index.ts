import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    name: "InfoLanding",
    component: () => import("@/modules/public/info/pages/landing/InfoLanding.vue"),
  },
  {
    path: "about",
    name: "InfoAbout",
    component: () => import("@/modules/public/info/pages/about/InfoAbout.vue"),
  },
  {
    path: "privacy",
    name: "InfoPrivacy",
    component: () => import("@/modules/public/info/pages/privacy/InfoPrivacy.vue"),
  },
  {
    path: "terms",
    name: "InfoTerms",
    component: () => import("@/modules/public/info/pages/terms/InfoTerms.vue"),
  },
];

export default routes;
