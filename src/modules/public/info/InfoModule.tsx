import { defineComponent } from "vue";
import { IonRouterOutlet } from "@ionic/vue";

export default defineComponent({
  name: "InfoModule",
  setup() {
    return () => (
      <div>
        <IonRouterOutlet></IonRouterOutlet>
      </div>
    );
  },
});
