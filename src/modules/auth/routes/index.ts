import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    redirect: "/auth/login",
  },
  {
    path: "login",
    name: "Login",
    component: () => import("@/modules/auth/pages/login/AuthLogin.vue"),
  },
  {
    path: "signup/create-account",
    name: "CreateAccount",
    component: () => import("@/modules/auth/pages/signup/create-account/SignupCreateAccount.vue"),
  },
  {
    path: "signup/verify-registration",
    name: "VerifyRegistration",
    component: () => import("@/modules/auth/pages/signup/verifyRegistration/SignupVerifyRegistration.vue"),
  },
  {
    path: "forgot/request-otp",
    name: "RequestOtp",
    component: () => import("@/modules/auth/pages/forgot/request-otp/ForgotRequestOtp.vue"),
  },
  {
    path: "forgot/confirm-otp",
    name: "ConfirmOtp",
    component: () => import("@/modules/auth/pages/forgot/confirm-otp/ForgotConfirmOtp.vue"),
  },
];

export default routes;
