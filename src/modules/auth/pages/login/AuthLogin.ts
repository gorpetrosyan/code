import { IonPage, IonContent, IonButton, IonItem, IonLabel, IonInput, IonCol, IonRow, IonIcon } from "@ionic/vue";
import { securityApi } from "@/common/services/api";
import { useAuth } from "@/common/services/api/auth";
import { Form as VForm, Field as VField, ErrorMessage as VErrorMessage } from "vee-validate";
import { hardReload } from "@/router/helpers";
import { eye, eyeOff } from "ionicons/icons";
import { deviceInfoData } from "@/common/services/notifications/push/pushNotificationService/index";
import { ref, defineComponent } from "vue";
import { useRouter } from "vue-router";
import { errorToaster, successToaster } from "@/common/services/notifications/toaster/toasterService";
import SignUpComponent from "@/widgets/components/pages/SignUpComponent.vue";
import ForgotPasswordComponent from "@/widgets/components/pages/ForgotPasswordComponent.vue";
import { useStore } from "@/use/useStore";
import { AllActionTypes } from "@/store/actionTypes";
import {FcmService} from "@/services/fcmService";
export default defineComponent({
  name: "AuthLogin",
  components: {
    IonPage,
    IonContent,
    IonButton,
    IonItem,
    IonLabel,
    IonInput,
    IonCol,
    IonRow,
    VField,
    VForm,
    VErrorMessage,
    IonIcon,
    SignUpComponent,
    ForgotPasswordComponent,
  },
  setup() {
    const email = ref("");
    const password = ref("");
    const rememberMe = ref(true);
    const showPassword = ref(false);
    const router = useRouter();
    const store = useStore();
    /**
     * submit login
     */
    async function onSubmit() {
      securityApi
        .login(email.value.trim(), password.value, rememberMe.value)
        .then((data: any) => {
          if (data.access_token) {
            store.dispatch(AllActionTypes.GET_USER, {
              username: data.username,
              userId: data.userId,
              email: data.email,
              scope: data.scope,
              fresh: data.fresh,
            });
            const { setWelcome } = useAuth();
            if (data.fresh) {
              setWelcome("1");
              router.push({
                name: "CreatePlayerPhoto",
                params: { userId: data.userId },
              });
              successToaster("Login successfully!");
              deviceInfoData();
            } else {
              setWelcome("0");
              hardReload("PlaygroundLanding");
            }
            new FcmService().initPush();
          } else {
            if (data.error === "403") {
              if (data.error_description === "INACTIVE") {
                router.push({
                  name: "VerifyRegistration",
                  params: { email: email.value.trim() },
                });
                return;
              } else if (data.error_description === "DELETED") {
                errorToaster("Account has been deleted");
              } else if (data.error_description === "SUSPENDED") {
                errorToaster("You are suspended");
                router.push({
                  name: "SettingsSupportContact",
                });
              }
            } else {
              errorToaster("Invalid login credentials");
            }
          }
        })
        .catch((error: any) => {
          errorToaster(error.message);
        });
    }

    /**
     * hide or show password
     */
    function onPasswordToggle() {
      showPassword.value = !showPassword.value;
    }

    /**
     * check input validation
     * @param value
     * @param key
     */
    function isRequired(value: any, key: any) {
      if (!value) {
        return "This field is required";
      }
      return true;
    }
    return {
      email,
      password,
      rememberMe,
      showPassword,
      onSubmit,
      onPasswordToggle,
      isRequired,
      eye,
      eyeOff,
    };
  },
});
