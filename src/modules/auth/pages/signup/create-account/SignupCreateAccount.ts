import { IonPage, IonContent, IonRow, IonCol, IonButton, IonItem, IonLabel, IonInput, IonDatetime } from "@ionic/vue";
import * as V from "vee-validate/dist/vee-validate";
import { securityApi } from "@/common/services/api";
import RedirectBackComponent from "@/widgets/components/pages/RedirectBackComponent.vue";
import { ref, defineComponent } from "vue";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";
import { useRouter } from "vue-router";
import { globalDateFormat } from "@/mixins/timeFormat";

export default defineComponent({
  name: "SignupCreateAccount",
  components: {
    IonPage,
    IonButton,
    IonItem,
    IonContent,
    IonLabel,
    IonInput,
    IonRow,
    IonCol,
    VField: V.Field,
    VForm: V.Form,
    VErrorMessage: V.ErrorMessage,
    IonDatetime,
    RedirectBackComponent,
  },
  setup() {
    const firstName = ref("");
    const lastName = ref("");
    const password = ref("");
    const confirmPassword = ref("");
    const email = ref("");
    const dob = ref("");
    const rememberMe = ref(false);
    const router = useRouter();
    /**
     * submit sign up
     */
    async function onSubmit() {
      const res = await securityApi.checkEmailAvailability(email.value);
      if (res.errorCode) {
        await errorToaster("Email already exists");
        return;
      } else if (password.value != confirmPassword.value) {
        await errorToaster("Passwords must match");
        return;
      } else {
        await securityApi.register(
          email.value,
          firstName.value,
          lastName.value,
          (dob.value = globalDateFormat(dob.value, "YYYY-MM-DD")),
          password.value,
        );
        router.push({
          path: "",
          name: "VerifyRegistration",
          params: {
            email: email.value,
          },
        });
      }
    }

    /**
     * checkout validation
     * @param value
     * @param key
     */
    function isRequired(value: any, key: any) {
      if (!value) {
        return key.field + " is required";
      }
      return true;
    }
    return {
      isRequired,
      onSubmit,
      email,
      password,
      confirmPassword,
      firstName,
      lastName,
      dob,
      rememberMe,
    };
  },
});
