import { IonPage, IonContent, IonButton, IonItem, IonLabel, IonInput, IonCol, IonRow, IonList } from "@ionic/vue";
import { securityApi } from "@/common/services/api";
import * as V from "vee-validate/dist/vee-validate";
import { deviceInfoData } from "@/common/services/notifications/push/pushNotificationService";
import RedirectBackComponent from "@/widgets/components/pages/RedirectBackComponent.vue";
import { ref, defineComponent } from "vue";
import { useRouter, useRoute } from "vue-router";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";
import { hardReload } from "@/router/helpers";
import {FcmService} from "@/services/fcmService";
export default defineComponent({
  name: "SignupVerifyRegistration",
  components: {
    IonPage,
    IonContent,
    IonButton,
    IonItem,
    IonLabel,
    IonInput,
    IonCol,
    IonRow,
    IonList,
    VField: V.Field,
    VForm: V.Form,
    VErrorMessage: V.ErrorMessage,
    RedirectBackComponent,
  },
  setup() {
    const otp = ref("");
    const router = useRouter();
    const route = useRoute();

    /**
     * validate inputs
     * @param value
     * @param key
     */
    function isRequired(value: any, key: any) {
      if (!value) {
        return key.field + " is required";
      }
      return true;
    }

    /**
     * submit opt
     */
    async function onSubmit() {
      const email: string | string[] = route.params.email;
      securityApi
        .verifyAccountOTP(otp.value, email)
        .then(() => {
          deviceInfoData();
          new FcmService().initPush();
          hardReload("CreatePlayerPhoto");
        })
        .catch(async () => {
          await errorToaster("Enter valid OTP");
        });
    }
    return {
      otp,
      isRequired,
      onSubmit,
    };
  },
});
