import { IonPage, IonContent, IonButton, IonItem, IonLabel, IonInput, IonCol, IonRow, IonList } from "@ionic/vue";
import { securityApi } from "@/common/services/api";
import * as V from "vee-validate/dist/vee-validate";
import RedirectBackComponent from "@/widgets/components/pages/RedirectBackComponent.vue";
import { ref, defineComponent } from "vue";
import { errorToaster, successToaster } from "@/common/services/notifications/toaster/toasterService";
import { useRouter } from "vue-router";
import * as yup from "yup";
export default defineComponent({
  name: "ForgotResetPassword",
  components: {
    IonPage,
    IonContent,
    IonButton,
    IonItem,
    IonLabel,
    IonInput,
    IonCol,
    IonRow,
    RedirectBackComponent,
    IonList,
    VForm: V.Form,
    VErrorMessage: V.ErrorMessage,
    VField: V.Field,
  },
  setup() {
    const email = ref("");
    const router = useRouter();
    /**
     * input validation
     * @param value
     * @param key
     */
    const validateEmail = yup.string().required("Email is required").email("Please enter a valid email");

    /**
     * submit otp recovery
     */
    async function onSubmit(): Promise<void> {
      securityApi
        .requestOTP(email.value)
        .then(() => {
          router.push({ name: "ConfirmOtp", params: { email: email.value } });
          successToaster("OTP sent successfully!");
          email.value = "";
        })
        .catch(() => {
          errorToaster("Please enter valid email");
        });
    }

    return {
      email,
      validateEmail,
      onSubmit,
    };
  },
});
