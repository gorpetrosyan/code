import { IonPage, IonContent, IonButton, IonItem, IonLabel, IonInput, IonCol, IonRow, IonList } from "@ionic/vue";
import { securityApi } from "@/common/services/api";
import * as V from "vee-validate/dist/vee-validate";
import RedirectBackComponent from "@/widgets/components/pages/RedirectBackComponent.vue";
import { ref, defineComponent } from "vue";
import { errorToaster, successToaster } from "@/common/services/notifications/toaster/toasterService";
import { useRouter, useRoute } from "vue-router";
export default defineComponent({
  name: "ForgotConfirmOtp",
  components: {
    IonPage,
    IonContent,
    IonButton,
    IonItem,
    IonLabel,
    IonInput,
    RedirectBackComponent,
    IonCol,
    IonRow,
    IonList,
    VField: V.Field,
    VForm: V.Form,
    VErrorMessage: V.ErrorMessage,
  },
  setup() {
    const router = useRouter();
    const route = useRoute();
    const otp = ref("");
    const newPassword = ref("");
    const password = ref("");

    /**
     * input validation
     * @param value
     * @param key
     */
    function isRequired(value: any, key: any): string | boolean {
      if (!value) {
        return key.field + " is required";
      }
      return true;
    }

    /**
     * confirm password
     * @param value
     * @param key
     */
    function isMatching(value: string, key: any): string | boolean {
      if (!value) {
        return key.field + " is required";
      } else if (value !== newPassword.value) {
        errorToaster("Passwords should match");
        return "Passwords should match";
      }
      return true;
    }

    /**
     * submit forgot confirm password
     */
    async function onSubmit(): Promise<void> {
      const email: string | string[] = route.params.email;
      securityApi
        .changePassword(otp.value, email, password.value)
        .then(() => {
          successToaster("Password changed successfully!");
          router.push({ name: "Login" });
        })
        .catch(() => {
          errorToaster("Enter valid OTP");
        });
    }
    return {
      otp,
      newPassword,
      password,
      isRequired,
      isMatching,
      onSubmit,
    };
  },
});
