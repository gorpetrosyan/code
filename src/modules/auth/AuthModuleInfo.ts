import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const AuthModuleInfo = {
  [moduleNames.AUTHENTICATION]: {
    path: import("@/modules/auth/AuthModule"),
    routes,
    state: {},
  },
};

export { AuthModuleInfo };
