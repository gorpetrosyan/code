import { defineComponent } from "vue";
import { IonRouterOutlet } from "@ionic/vue";

export default defineComponent({
  name: "AuthModule",
  setup() {
    const styles = `  

        // Responsive

        @media screen and (min-width: 720px) and (max-width: 992px) {
          .auth {
            margin-left: 15%;
            width: 70%;
          }
        }
        @media screen and (min-width: 992px) {
          .auth {
            margin-left: 25%;
            width: 50%;
          }
        }

    `;
    return () => (
      <div>
        <style scoped>{styles}</style>
        <IonRouterOutlet class="auth"></IonRouterOutlet>
      </div>
    );
  },
});
