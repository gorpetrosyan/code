import { defineComponent } from "vue";
import { IonRouterOutlet } from "@ionic/vue";

export default defineComponent({
  name: "SocialMediaModule",
  setup() {
    return () => (
      <div>
        <IonRouterOutlet></IonRouterOutlet>
      </div>
    );
  },
});
