import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    name: "PlaygroundLanding",
    component: () => import("@/modules/dashboard/social-media/pages/playground/landing/PlaygroundLanding.vue"),
  },

  // Posts
  {
    path: "post/create",
    name: "CreateSocialPost",
    component: () => import("@/modules/dashboard/social-media/pages/post/create/SocialPostCreate.vue"),
  },
  {
    path: "post/view/:refId",
    name: "ViewSocialPost",
    component: () => import("@/modules/dashboard/social-media/pages/post/view/SocialPostView.vue"),
  },
  {
    path: "post/comments/:reference",
    name: "SocialPostComments",
    component: () => import("@/modules/dashboard/social-media/pages/post/comments/SocialPostComments.vue"),
  },
  {
    path: "post/likes/:reference",
    name: "SocialPostLikes",
    component: () => import("@/modules/dashboard/social-media/pages/post/likes/SocialPostLikes.vue"),
  },
  {
    path: "post/shares/:refId",
    name: "SocialPostShare",
    component: () => import("@/modules/dashboard/social-media/pages/post/share/SocialPostShares.vue"),
  },
];

export default routes;
