import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const SocialMediaModuleInfo = {
  [moduleNames.SOCIAL_MEDIA]: {
    path: import("@/modules/dashboard/social-media/SocialMediaModule"),
    routes,
    state: {},
  },
};

export { SocialMediaModuleInfo };
