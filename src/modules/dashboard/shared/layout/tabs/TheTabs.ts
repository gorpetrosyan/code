import { IonTabBar, IonTabButton, IonTabs, IonContent, IonIcon, IonPage } from "@ionic/vue";

import { homeOutline, chatboxEllipsesOutline, notificationsOutline, searchOutline, addOutline } from "ionicons/icons";

// import { useRouter } from "vue-router";
// import { navOptions } from "@/config/modules/navs";
import { computed, onMounted, ref, watch } from "vue";

import { useLocalStorage } from "@/common/services/storage/localStorageService";
import { WELCOME } from "@/common/services/api/auth";
import { socialApi } from "@/common/services/api";
import { useKeyboard } from '@ionic/vue';
export default {
  name: "TheTabs",
  components: {
    IonContent,
    IonTabs,
    IonTabBar,
    IonTabButton,
    IonIcon,
    IonPage,
  },
  setup() {
    // Dynamic Tab Nav Bar
    // const router = useRouter();
    // const getRoutePath = computed((page: string) => {
    //   return router.resolve({
    //     name: page,
    //   }).href;
    // });
    const { isOpen, keyboardHeight } = useKeyboard();
    watch(isOpen, () => { 
      console.log(`Is Keyboard Open: ${isOpen.value}, Keyboard Height: ${keyboardHeight.value}`);
    });
    const getProfileCompleted = () => {
      const isCompleted = useLocalStorage(WELCOME).value === "0";
      return isCompleted;
    };

    const notificationsCount = ref("");

    async function fetchNotificationCount() {
      const count = await socialApi.getUnseenNotificationCount();
      if (count) {
        notificationsCount.value = count > 9 ? "9+" : count;
      } else {
        notificationsCount.value = "";
      }
    }

    function getNotificationCount(timeout = 0) {
      if (timeout) {
        setTimeout(() => {
          fetchNotificationCount();
        }, timeout);
      } else {
        fetchNotificationCount();
      }
    }
   
    onMounted(() => {
      getNotificationCount();
    });

    return {
      // navOptions: navOptions(),
      // getRoutePath,
      notificationsOutline,
      homeOutline,
      chatboxEllipsesOutline,
      searchOutline,
      addOutline,
      getProfileCompleted,
      getNotificationCount,
      notificationsCount,
      isOpen,
    };
  },
};
