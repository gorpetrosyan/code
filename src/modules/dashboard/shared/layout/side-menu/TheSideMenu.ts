import {
  IonApp,
  IonContent,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuButton,
  IonBackButton,
  IonButtons,
  IonTitle,
  IonToolbar,
  IonHeader,
  IonAvatar,
  IonItemGroup,
  IonIcon,
  IonMenuToggle,
  IonNote,
  IonSplitPane,
  menuController,
} from "@ionic/vue";
import { defineComponent } from "vue";
import { useRoute, RouterLink } from "vue-router";

import { WELCOME } from "@/common/services/api/auth";
import TheTabs from "../tabs/TheTabs.vue";

import { navOptions } from "@/config/modules/navs";
import { securityApi, profileApi } from "@/common/services/api";
import { hardReload } from "@/router/helpers";
import { clearFromLocalStorage, useLocalStorage } from "@/common/services/storage/localStorageService/index";

import ProfileCard from "@/modules/dashboard/shared/components/profile-card/ProfileCard.vue";
import { useKeyboard } from '@ionic/vue';
export default defineComponent({
  name: "TheSideMenu",
  components: {
    IonApp,
    ProfileCard,
    IonContent,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonMenu,
    IonMenuButton,
    IonBackButton,
    IonButtons,
    IonTitle,
    IonToolbar,
    IonHeader,
    IonMenuToggle,
    IonNote,
    IonSplitPane,
    IonAvatar,
    IonItemGroup,
    IonIcon,
    TheTabs,
    RouterLink,
  },
  data() {
    const AUTH_KEY = "user_id";
    const loggedIn = !!useLocalStorage(AUTH_KEY).value; //weather user is logged in with credentials.
    const firstTime = useLocalStorage(WELCOME).value === "1";
    const route = useRoute();

    const getProfileCompleted = () => {
      const isCompleted = useLocalStorage(WELCOME).value === "0";
      return isCompleted;
    };
    const { isOpen, keyboardHeight } = useKeyboard();
    // watch(isOpen, () => { 
    //   console.log(`Is Keyboard Open: ${isOpen.value}, Keyboard Height: ${keyboardHeight.value}`);
    // });

    return {
      navOptions: navOptions(),
      avtar: require("@/assets/icons/avtar.png"),
      sidebar: loggedIn && !firstTime,
      submenuchek: false,
      profiles: [],
      cprofile: {
        id: 0,
        name: "",
        kind: "Player",
      },
      isSelected: (url: string) => (url === route.name ? "selected" : ""),
      getProfileCompleted,
      isOpen,
    };
  },
  
  mounted: async function () {
    await this.getProfileView();
  },
  methods: {
    setAltImg(event: any) {
      event.target.src = this.avtar;
    },
    getProfileView: async function () {
      const data = await securityApi.me();
      if (data) {
        const id = profileApi.getProfileId();
        const at = data.profiles.findIndex((x: any) => x.id === id);
        const pf = data.profiles[at];
        pf.kind = profileApi.getProfileKind(pf.type);
        this.cprofile = pf;
        data.profiles.splice(at, 1);
        this.profiles = data.profiles;
      }
    },
    getProfileType(key: string) {
      if (key === "U") return "Player";
      if (key === "C") return "Coach";
      if (key === "V") return "Venue";
      if (key === "T") return "Club";
      if (key === "TM") return "Team";
      return "Player";
    },
    logout() {
      clearFromLocalStorage();
      hardReload("Login");
    },
    close() {
      menuController.close();
    },
    submenu() {
      this.submenuchek = !this.submenuchek;
    },
    switchProfile(p: any) {
      const CHAT_TOKEN = "chat_token";
      clearFromLocalStorage(CHAT_TOKEN);
      profileApi.setProfile(p);
    },
  },
});
