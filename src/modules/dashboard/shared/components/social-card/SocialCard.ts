import { defineComponent } from "vue";
import {
  IonItem,
  IonList,
  IonLabel,
  IonCol,
  IonRow,
  IonText,
  IonSlide,
  IonSlides,
  IonAvatar,
  IonButton,
  toastController,
  loadingController,
  popoverController,
  actionSheetController,
  modalController,
} from "@ionic/vue";
import SocialCardOptions from "@/modules/dashboard/shared/components/modals/social-card-options/SocialCardOptions.vue";
import SocialReportModal from "@/modules/dashboard/shared/components/modals/social-card-report/SocialReportModal.vue";
import moment from "moment";
import { profileApi } from "@/common/services/api";
import { close, open } from "ionicons/icons";
import postTypes from "@/common/types/post-types";
import { hardReload } from "@/router/helpers";

export default defineComponent({
  name: "SocialCard",
  components: {
    IonItem,
    IonList,
    IonLabel,
    IonCol,
    IonRow,
    IonText,
    IonSlide,
    IonSlides,
    IonAvatar,
    IonButton,
  },
  props: {
    item: {
      type: Object,
      default: null,
    },
  },
  emits: ["social"],
  data() {
    return {
      missing: require("@/assets/icons/missing.png"),
      timeout: 1000,
      slideOpts: {
        speed: 1000,
        navigation: true,
        autoplay: false,
      },
      postTypes,
    };
  },
  methods: {
    setAltImg(event: any) {
      console.log("img-error", event);
      event.target.src = this.missing;
    },
    getKind(type: string) {
      return profileApi.getProfileKind(type);
    },
    formatTime(value: any) {
      if (value) {
        //return moment(String(value)).format("MMMM Do YYYY, h:mm a");
        return moment(String(value)).fromNow();
      }
    },
    // Toaster
    async openToast(message: any) {
      const toast = await toastController.create({
        message: message,
        duration: 2000,
        position: "top",
      });
      return toast.present();
    },

    //Loader
    async presentLoading(type: any) {
      const load = await loadingController.create({
        cssClass: "customloader",
        message: "Please wait...",
        duration: this.timeout,
      });
      if (type == true) {
        await load.present();
      } else {
        load.dismiss();
        this.timeout = 10;
      }
    },
    socialevents(item: any, type: string) {
      console.log(item, type);
      if (type == "like") {
        item.liked == false
          ? (item.noOfLikes = item.noOfLikes + 1)
          : item.noOfLikes == 0
          ? (item.noOfLikes = 0)
          : (item.noOfLikes = item.noOfLikes - 1);
      }
      this.$emit("social", { item, type });
    },
    openPost(item: any) {
      this.$router.push({ name: "ViewSocialPost", params: { refId: item.referenceId } });
    },
    pushtolike(items: any) {
      this.$router.push({ name: "SocialPostLikes", params: { reference: items.referenceId } });
    },
    async openPopover(ev: Event, id: string) {
      const popover = await popoverController.create({
        component: SocialCardOptions,
        componentProps: {
          id: id,
          onClick: () => {
            popover.dismiss();
          },
        },
        event: ev,
        translucent: true,
      });
      return popover.present();
    },
    async reportoptions(id: any) {
      const actionSheet = await actionSheetController.create({
        header: "Report User",
        buttons: [
          {
            text: "Report",
            icon: open,
            handler: () => {
              const data = {
                id: id,
                type: "U",
              };
              this.reportsModal(data);
            },
          },
          {
            text: "Cancel",
            icon: close,
            role: "cancel",
          },
        ],
      });
      return actionSheet.present();
    },
    async reportsModal(reports: any) {
      const reportmodal = await modalController.create({
        component: SocialReportModal,
        mode: "ios",
        componentProps: {
          items: reports,
        },
      });
      reportmodal.onDidDismiss().then((data) => {
        console.log("test", data);
        if (data.data == true) {
          hardReload();
        }
      });
      return reportmodal.present();
    },
    truncate(input: string) {
      if (input.length > 70) {
        return input.substring(0, 70) + "...";
      }
      return input;
    },
    getPostTitle(post: any) {
      let postTitle = "";
      switch (post.postType) {
        case postTypes.EVENT:
          postTitle = "Event: ";
          break;

        case postTypes.GAME:
          postTitle = "Game: ";
          break;

        case postTypes.TOURNAMENT:
          postTitle = "Tournament: ";
          break;

        case postTypes.CHALLENGE:
          postTitle = "Challenge: ";
          break;

        default:
          postTitle = "";
          break;
      }
      if (postTitle) {
        postTitle = postTitle + " - " + post.eventDetails?.description;
      }
      postTitle = post.text + postTitle;
      return postTitle;
    },
  },
});
