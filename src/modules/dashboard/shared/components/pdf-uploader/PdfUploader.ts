import { onMounted } from "vue";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";

import {
  mediaLimitMax,
  mediaTypes,
  storageType,
} from "@/config/properties/file-upload/file-upload-settings";

interface SetupContext {
  emit: (event: string, ...args: object[]) => void;
}

import { storeFile } from "@/common/services/file/fileStoreService";
import { UploadResult } from "@/common/models";

export default {
  name: "PdfUploader",
  components: {},
  props: {
    multiple: {
      type: Boolean,
      default: false,
    },
    storageType: {
      type: String,
      validator(value: string) {
        return Object.values(storageType).indexOf(value) !== -1;
      },
      default: storageType.PRIVATE,
    },
  },
  emits: ["export-uploaded-data"],
  setup(props: any, context: SetupContext) {
    let limit = 1;

    /**
     *  returns void
     *  emitted the file data to parent component
     *  @param fileObjectToEmit
     **/
    function emitUploadedDataFunction(fileObjectToEmit: UploadResult): void {
      context.emit("export-uploaded-data", fileObjectToEmit);
    }

    /**
     * Upload pdf from file storage ios/android
     * return false/true
     **/
    function uploadPdf() {
      const pdfInput: any = document.getElementById("openFiles");
      pdfInput.click();
    }

    /**
     * set limitation on uploaded files
     * return void
     **/
    function fileLimitation(): void {
      props.multiple ? (limit = mediaLimitMax) : (limit = 1);
    }

    /**
     * get out file url
     * @param file
     * @param type
     * return any
     **/
    async function getUploadedFileUrl(file: any, type: string): Promise<any> {
      return new Promise<any>((resolve, reject) => {
        const response: { [key: string]: any } = {
          url: null,
          fileType: type,
          storageType: props.storageType,
          key: null,
        };
        const reader: any = new FileReader();
        reader.onload = async function (e: any): Promise<any> {
          await storeFile(e.target.result, type, props.storageType, props.allowCompress)
            .then((result: any) => {
              response.url = result.url;
              response.key = result.key;
              resolve(response);
            })
            .catch((e: any) => {
              errorToaster("Something went wrong, please try again");
              console.log(e);
              reject(e);
            });
        };
        reader.readAsDataURL(file);
      });
    }

    /**
     * hidden input file value checkout and sort by category
     * @param event
     * return void
     **/
    function resetMedia(event: any) {
      event.target.files = null;
      event.target.value = null;
    }

    async function getMedia(event: any): Promise<void> {
      const files = event.target.files;
      // console.log(files, "before");
      if (files.length <= limit) {
        await files.forEach((file: any) => {
          // console.log(file, "before");
          getUploadedFileUrl(file, "pdf")
            .then((result) => {
              console.log("here", result);
              emitUploadedDataFunction({
                url: result.url,
                fileType: mediaTypes.PDF,
                storageType: props.storageType,
                key: result.key,
              } as UploadResult);
            })
            .catch((e) => {
              errorToaster("Please try again");
            });
        });
      } else {
        await errorToaster("Maximum content limit");
      }
    }

    /**
     * ------------------------------------- hooks part ----------------------------------------------------------
     **/
    onMounted(() => {
      fileLimitation();
    });
    return {
      resetMedia,
      getMedia,
      uploadPdf,
      click: () => {
        uploadPdf();
      },
    };
  },
};
