# File Upload Component Service

This README would normally document whatever steps are necessary to get your application up and running.

### What is this component for?

- Quick summary
- API

### How do I get set up?

- import FileUpload component from common/components/fileUpload/FileUploader
- use as <file-uploader></file-uploader>
- set properties
- emmit the event

### properties

- "media-type" property, receives 4 values with type of string( image, media, video, photo),
- "multiple" property, receives boolean value
- "storageType" property, receives 2 values(public,private)
- "image-crop" property, receives boolean value

### emitter

- 'exportUploadedData' event emitter

### media-type Property description

- "image" value allows getting only images from device gallery
- "media" value allows getting both images and videos from gallery
- "video" value allows getting video from device camera or from gallery
- "photo" value allows getting photo from device camera or from gallery

### multiple Property description

- false value gets single file
- true value gets multiple files

### storageType Property description

- public value return public accessibility files
- private value return private accessibility files

### image-crop Property description

- false value disabled file cropping(default)
- true value enabled file cropping

### exportUploadedData event description

- event return object callback with media(s) data and works while trigger click event on component
