import {ref, onMounted} from "vue";
import {Plugins, CameraResultType, CameraSource, Capacitor} from "@capacitor/core";
import {MediaCapture} from "@ionic-native/media-capture";
import {encodeVideo, storeFile} from "@/common/services/file/fileStoreService";
import {dataURItoBlob} from "@/common/services/file/helpers/dataURItoBlob";
import {cropImage} from "@/common/services/file/imageCropService";
import {UploadResult, VideoOutput} from "@/common/models";
import {errorToaster} from "@/common/services/notifications/toaster/toasterService";
import ImageCropperModal from "@/modules/dashboard/shared/components/image-cropper-modal/ImageCropperModal.vue";
import {modalController} from "@ionic/vue";

import {
  componentMediaPropsTypes,
  mediaFileTypesAndSize,
  mediaLimitMax,
  mediaTypes,
  photoQuality,
  photoSize,
  storageType,
  videoDuration,
  videoQuality,
  videoSize,
} from "@/config/properties/file-upload/file-upload-settings";

const {Camera} = Plugins;

interface SetupContext {
  emit: (event: string, ...args: object[]) => void;
}

export default {
  name: "FileUploader",
  components: {},
  props: {
    multiple: {
      type: Boolean,
      default: false,
    },
    mediaType: {
      type: String,
      validator(value: string) {
        return Object.values(componentMediaPropsTypes).indexOf(value) !== -1;
      },
      default: "image",
    },
    storageType: {
      type: String,
      validator(value: string) {
        return Object.values(storageType).indexOf(value) !== -1;
      },
      default: storageType.PRIVATE,
    },
    imageCrop: {
      type: Boolean,
      default: false,
    },
    imageAspectRatio: {
      type: Number,
      default: 1,
    },
    allowCompress: {
      type: Boolean,
      default: true,
    },
    config: {
      type: Object,
      default: () => {
        return {};
      },
    },
  },
  emits: ["export-uploaded-data"],
  setup(props: any, context: SetupContext) {
    let limit = 1;
    const acceptFiles = ref("");
    let cropModal;

    /**
     *  returns void
     *  emitted the file data to parent component
     *  @param fileObjectToEmit
     **/
    function emitUploadedDataFunction(fileObjectToEmit: UploadResult): void {
      context.emit("export-uploaded-data", fileObjectToEmit);
    }


    /**
     * Checkout what type of media upload
     * return void
     **/
    function getMediaType(): void {
      if (props.mediaType === "image") {
        acceptFiles.value = "image/png, image/jpeg, image/bmp";
      } else if (props.mediaType === "video") {
        acceptFiles.value = "video/mp4, video/webm, video/ogg, image/png, image/jpeg, image/bmp";
      } else if (props.mediaTypes === "pdf") {
        acceptFiles.value = "pdf";
      }
    }

    /**
     * @param base64
     * return file size
     **/
    function returnBase64FileSize(base64: string | any): number {
      const fileStringLength = base64.substring(base64.indexOf(",") + 1).length;
      return 4 * Math.ceil(fileStringLength / 3);
    }

    /**
     * @param imageFile
     * return any
     **/
    async function showCroppedImageModal(imageFile: any): Promise<any> {
      cropModal = await modalController.create({
        component: ImageCropperModal,
        cssClass: "full-screen-modal",
        componentProps: {
          file: imageFile,
          aspectRatio: props.imageAspectRatio,
        },
      });
      cropModal.present();
      // wait for a response when closing the modal
      return await cropModal.onDidDismiss().then(({data, role}) => {
        if (role) {
          return data;
        }
      });
    }

    /**
     * Take photo from camera/gallery ios/android
     * return promise
     **/
    async function takePicture(): Promise<void> {
      try {
        await Camera.getPhoto({
          resultType: CameraResultType.DataUrl, // the response of Camera's result
          source: CameraSource.Prompt, //the image is taken from device's camera or from gallery
          quality: photoQuality, // Quality of image
          allowEditing: false, // Allow user to edit the pic
          saveToGallery: false, // Allows user to save image into device
        })
          .then(async (photo) => {
            if (!["jpeg", "png", "bmp"].includes(photo.format)) {
              errorToaster("The file must be in (jpeg,png,bmp) format");
              return;
            }
            if (returnBase64FileSize(photo.dataUrl) > photoSize) {
              errorToaster("The file must be less or 10MB");
              return;
            }
            if (props.imageCrop) {
              console.log(photo.dataUrl, "before");
              photo.dataUrl = await showCroppedImageModal(photo.dataUrl);
            }
            storeFile(photo.dataUrl, mediaTypes.IMAGE, props.storageType, props.config, props.allowCompress)
              .then((result: any) => {
                emitUploadedDataFunction({
                  url: result.url,
                  fileType: mediaTypes.IMAGE,
                  storageType: props.storageType,
                  key: result.key,
                  thumbnail: result.url,
                  thumbnailKey: result.key,
                } as UploadResult);
              })
              .catch((e: any) => {
                errorToaster("Something went wrong, please try again");
              });
          })
          .catch((e) => {
            errorToaster("Something went wrong, please try again");
          });
      } catch (e) {
        await errorToaster("Turn on Camera Please");
      }
    }

    async function handleVideoFile(key: string, url: string) {
      encodeVideo(key).then((enc: VideoOutput) => {
        emitUploadedDataFunction({
          url,
          fileType: mediaTypes.VIDEO,
          storageType: props.storageType,
          key,
          thumbnail: enc.thumbnail.url,
          original: enc.original.url,
          mobile: enc.mobile.url,
          thumbnailKey: enc.thumbnail.key,
          originalKey: enc.original.key,
          mobileKey: enc.mobile.key,
        });
      });
    }

    /**
     * Upload file from files ios/android
     * return false/true
     **/
    function uploadFile() {
      const fileInput: any = document.getElementById("openFiles");
      fileInput.click();
      return true;
    }

    /**
     * Take video from camera/gallery ios/android
     * return Promise
     **/
    async function takeVideo(): Promise<any> {
      if (Capacitor.isNative) {
        try {
          const options = {
            limit: 1,
            duration: videoDuration,
            quality: videoQuality / 10,
          };
          await MediaCapture.captureVideo(options)
            .then((videos: any) => {
              const currentVideo = videos[0] ?? {fullPath: ""};
              if (returnBase64FileSize(currentVideo.fullPath) > videoSize) {
                errorToaster("The file must be less or 50MB");
                return;
              }
              storeFile(currentVideo.fullPath, mediaTypes.VIDEO, props.storageType, props.config, props.allowCompress)
                .then((result: any) => {
                  handleVideoFile(result.key, result.url);
                })
                .catch((e: any) => {
                  errorToaster("Something went wrong, please try again");
                  console.log(e);
                });
            })
            .catch((e) => {
              errorToaster("Please turn on Video Camera");
              console.log(e);
            });
        } catch (e) {
          await errorToaster("Please turn on Video Camera");
          console.log(e);
        }
      } else {
        uploadFile();
      }
    }

    /**
     * upload media with given type
     * @param fileType
     **/
    function uploadMediaFile(fileType: string) {
      const methodTypes: { [key: string]: any } = {
        pdf: uploadFile,
        photo: takePicture,
        image: uploadFile,
        video: takeVideo,
        media: uploadFile,
      };
      methodTypes[fileType]();
    }

    /**
     * set limitation on uploaded files
     * return void
     **/
    function fileLimitation(): void {
      props.multiple ? (limit = mediaLimitMax) : (limit = 1);
    }

    /**
     * get out file url
     * @param file
     * @param type
     * return any
     **/
    async function getUploadedFileUrl(file: any, type: string): Promise<any> {
      return new Promise<any>((resolve, reject) => {
        const response: { [key: string]: any } = {
          url: null,
          fileType: type,
          storageType: props.storageType,
          key: null,
        };
        const reader: any = new FileReader();
        reader.onload = async function (e: any): Promise<any> {
          if (props.imageCrop && Capacitor.isNative) {
            cropImage(e.target.result).then((res) => {
              e.target.result = res;
            });
          }
          await storeFile(e.target.result, type, props.storageType, props.config, props.allowCompress)
            .then((result: any) => {
              response.url = result.url;
              response.key = result.key;
              resolve(response);
            })
            .catch((e: any) => {
              errorToaster("Something went wrong, please try again");
              console.log(e);
              reject(e);
            });
        };
        reader.readAsDataURL(file);
      });
    }

    /**
     * hidden input file value checkout and sort by category
     * @param event
     * return void
     **/
    function resetMedia(event: any) {
      event.target.files = null;
      event.target.value = null;
    }

    async function getMedia(event: any): Promise<void> {
      let files = event.target.files;
      // console.log(files, "before");
      if (
        !props.multiple &&
        mediaFileTypesAndSize[files[0].type]["type"] === mediaTypes.IMAGE &&
        props.imageCrop &&
        !Capacitor.isNative
      ) {
        const file = dataURItoBlob(await showCroppedImageModal(files[0]));
        // console.log(file, "before");
        files = [file];
      }
      if (files.length <= limit) {
        await files.forEach((file: any) => {
          const itemId = mediaFileTypesAndSize[file.type];
          if (file.size <= itemId["size"]) {
            // console.log(file, "before");
            getUploadedFileUrl(file, itemId["type"])
              .then((result) => {
                if (itemId["type"] === mediaTypes.VIDEO) {
                  // request encoding.
                  handleVideoFile(result.key, result.url);
                } else if (itemId["type"] === mediaTypes.IMAGE) {
                  emitUploadedDataFunction({
                    url: result.url,
                    fileType: mediaTypes.IMAGE,
                    storageType: props.storageType,
                    key: result.key,
                    thumbnail: result.url,
                    thumbnailKey: result.key,
                  } as UploadResult);
                } else if (itemId["type"] === mediaTypes.PDF) {
                  emitUploadedDataFunction({
                    url: result.url,
                    fileType: mediaTypes.PDF,
                    storageType: props.storageType,
                    key: result.key,
                  } as UploadResult);
                } else {
                  errorToaster("Unknown file type");
                }
              })
              .catch((e) => {
                errorToaster("Please try again");
              });
          } else {
            errorToaster("Maximum content size");
          }
        });
      } else {
        await errorToaster("Maximum content limit");
      }
    }

    /**
     * ------------------------------------- hooks part ----------------------------------------------------------
     **/

    onMounted(() => {
      fileLimitation();
      getMediaType();
    });
    return {
      uploadMediaFile,
      resetMedia,
      getMedia,
      acceptFiles,
      click: () => {
        uploadMediaFile(props.mediaType);
      },
    };
  },
};
