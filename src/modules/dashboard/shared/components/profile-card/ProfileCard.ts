import { defineComponent } from "vue";
import { IonAvatar, IonItem, IonText, IonNote } from "@ionic/vue";

import Config from "@/config/environment";
const baseUrl = Config.API_URL;

export default defineComponent({
  name: "ProfileCard",
  components: {
    IonAvatar,
    IonItem,
    IonText,
    IonNote,
  },
  props: {
    profileId: {
      type: Number,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    message: {
      type: String,
      default: "",
    },
    theme: {
      type: String,
      default: "main",
    },
    action2: {
      type: String,
      default: "",
    },
    action1: {
      type: String,
      default: "",
    },
    note: {
      type: String,
      default: "",
    },
  },
  emits: ["action"],
  computed: {
    imageUrl: function (): string {
      console.log(`${baseUrl}user/profiles/${this.$props.profileId}/avatar`);
      return `${baseUrl}user/profiles/${this.$props.profileId}/avatar`;
    },
  },
  methods: {
    setDefaultImg(event: any) {
      const getImage = require.context("@/assets/icons/", false, /\.png$/);
      event.target.src = getImage("./avtar.png");
    },
    onAct(num: number) {
      this.$emit("action", { kind: num });
    },
    getProfileType(key: string) {
      if (key === "U") return "Player";
      if (key === "C") return "Coach";
      if (key === "V") return "Venue";
      if (key === "T") return "Club";
      if (key === "TM") return "Team";
      return "Player";
    },
  },
});
