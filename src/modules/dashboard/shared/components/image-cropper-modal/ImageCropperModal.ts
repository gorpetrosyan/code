import {
  IonContent,
  IonHeader,
  IonTitle,
  IonToolbar,
  modalController,
  IonRow,
  IonCol,
  IonButton,
  IonIcon,
} from "@ionic/vue";
import { defineComponent, ref, watchEffect, onBeforeUpdate } from "vue";
import {
  cropOutline,
  search,
  addCircleOutline,
  removeCircleOutline,
  imageOutline,
  arrowUndoCircleOutline,
  arrowRedoCircleOutline,
  arrowBackOutline,
  arrowForwardOutline,
  arrowUpOutline,
  arrowDownOutline,
} from "ionicons/icons";

import VueCropper from "vue-cropperjs";
import "cropperjs/dist/cropper.css";

import { errorToaster } from "@/common/services/notifications/toaster/toasterService";
import { dataURItoBlob } from "@/common/services/file/helpers/dataURItoBlob";

export default defineComponent({
  name: "ImageCropperModal",
  components: {
    IonContent,
    IonHeader,
    IonTitle,
    IonToolbar,
    IonRow,
    IonCol,
    IonButton,
    IonIcon,
    VueCropper,
  },
  props: {
    file: {
      type: Object,
      required: true,
    },
    aspectRatio: {
      type: Number,
      default: 1,
    },
  },
  setup(props: any) {
    const imgSrc: any = ref("null");
    const cropImg: any = ref("null");
    const cropper: any = ref(null);
    const a = ref([]);
    const icons = ref({
      cropOutline,
      search,
      addCircleOutline,
      removeCircleOutline,
      imageOutline,
      arrowUndoCircleOutline,
      arrowRedoCircleOutline,
      arrowBackOutline,
      arrowForwardOutline,
      arrowUpOutline,
      arrowDownOutline,
    });

    /**
     * set image
     * */
    function setImage(): void {
      let file = props.file;
      if (typeof file == "string") {
        file = dataURItoBlob(file);
      }
      if (typeof FileReader === "function") {
        const reader = new FileReader();
        reader.onload = (event: any) => {
          imgSrc.value = event.target.result;
          cropper.value.replace(event.target.result);
        };
        reader.readAsDataURL(file);
      } else {
        errorToaster("Sorry, FileReader API not supported");
      }
    }
    setImage();

    /**
     * Zoom In/Out
     * @param percent
     */
    function zoomImage(percent: number) {
      cropper.value.relativeZoom(percent);
    }

    /**
     * rotate 360deg
     * @param deg
     */
    function rotateImage(deg: number) {
      cropper.value.rotate(deg);
    }

    /**
     * move up/down/left/right
     * @param offsetX
     * @param offsetY
     */
    function moveImage(offsetX: number, offsetY: number) {
      cropper.value.move(offsetX, offsetY);
    }

    /**
     * save cropped image and emit
     */
    function cropImage() {
      cropImg.value = cropper.value
        .getCroppedCanvas({
          width: 160,
          height: 90,
          minWidth: 256,
          minHeight: 256,
          maxWidth: 4096,
          maxHeight: 4096,
          fillColor: "#fff",
          imageSmoothingEnabled: false,
          imageSmoothingQuality: "high",
        })
        .toDataURL();
      // context.emit("export-cropped-image-data", cropImg);
    }

    /**
     * close modal
     * @param submit
     * */
    async function modalClose(submit: boolean): Promise<void> {
      cropImage();
      let croppedImage = null;
      let closeAction = "";
      if (submit) {
        croppedImage = cropImg.value;
        console.log(croppedImage, "croppedImage");
        closeAction = "cropped";
      }
      await modalController.dismiss(croppedImage, closeAction);
    }

    // Hooks

    watchEffect(
      () => {
        // the DOM element will be assigned to the ref after initial render
      },
      {
        flush: "post",
      },
    );

    onBeforeUpdate(() => {
      a.value = [];
    });

    return {
      imgSrc,
      cropper,
      modalClose,
      zoomImage,
      rotateImage,
      moveImage,
      cropImage,
      cropImg,
      icons,
      a,
    };
  },
});
