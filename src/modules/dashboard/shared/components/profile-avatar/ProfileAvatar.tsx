import { mediaApi } from "@/common/services/api";
import { IonAvatar } from "@ionic/vue";
import { defineComponent, ref, watch } from "vue";

export default defineComponent({
  name: "ProfileAvatar",
  components: {
    IonAvatar,
  },
  props: {
    imageKey: {
      type: String,
      required: true,
    },
  },
  setup(props) {
    const getImage = require.context("@/assets/icons/", false, /\.png$/);
    const img = getImage("./avtar.png");

    const url = ref("");
    url.value = img;

    function setDefaultImg(event: any) {
      event.target.src = img;
    }

    watch(
      () => props.imageKey,
      (val: string) => {
        if (val && val.length > 1) {
          console.log("Props", props.imageKey);
          if (props.imageKey.startsWith("http://") || props.imageKey.startsWith("https://")) {
            url.value = props.imageKey;
          } else {
            mediaApi.parseKey(props.imageKey).then((result) => {
              url.value = result.url;
            });
          }
        }
      },
    );

    return () => (
      <IonAvatar class="image-upload">
        <img src={url.value} onError={setDefaultImg} />
      </IonAvatar>
    );
  },
});
