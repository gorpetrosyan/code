import { genericApi, profileApi } from "@/common/services/api";

export default {
  name: "SportsSelectionList",
  props: {
    userSports: {
      type: Object,
      default: () => {
        return {};
      },
    },
    showSkills: {
      type: Boolean,
      default: false,
    },
    multiple: {
      type: Boolean,
      default: true,
    },
    userSport: {
      type: Number,
      default: 0,
    },
  },
  emits: ["set-sport", "select-sport"],
  data() {
    return {
      search: "",
      categories: [],
      sports: [],
      categoryFilter: "",
      selectedSports: {},
      skills: [],
      selectedSport: this.userSports,
    };
  },
  computed: {
    filteredList() {
      return this.sports.filter((sport) => {
        if (this.categoryFilter === "") {
          return sport.name.toLowerCase().includes(this.search.toLowerCase());
        } else {
          return (
            sport.name.toLowerCase().includes(this.search.toLowerCase()) &&
            sport.categories.includes(this.categoryFilter)
          );
        }
      });
    },
  },
  mounted() {
    this.getPlayerCategories();
    this.getSports();
    this.getSkills();
    this.selectedSports = this.userSports;
    this.selectedSport = this.userSport;
    this.sendSportsList();
  },
  methods: {
    removeSports: function (id) {
      delete this.selectedSports[id];
      this.sendSportsList();
    },
    setSport: function (sport) {
      if (!this.selectedSports[sport.sportId]) {
        this.selectedSports[sport.sportId] = {
          id: sport.sportId,
          name: sport.name,
          skillGroup: [1],
        };
      }
      this.sendSportsList();
    },
    setSelectedSport(id) {
      this.selectedSport = id;
      this.$emit("select-sport", id);
    },
    sendSportsList: function () {
      this.$emit("set-sport", this.selectedSports);
    },
    setCategory: function (categoryId) {
      this.categoryFilter = categoryId;
    },
    getPlayerCategories: async function () {
      const res = await profileApi.getPlayerCategories();
      this.categories = res.data;
    },
    getSports: async function () {
      const res = await profileApi.getPlayerSports();
      this.sports = res.data;
    },
    getSkills: async function () {
      this.skills = await genericApi.getSkillLevels();
    },
    setSkill: function (id, skill) {
      this.selectedSports[id].skillGroup = skill;
    },
    checkSelected: function (id) {
      return this.selectedSports[id];
    },
  },
};
