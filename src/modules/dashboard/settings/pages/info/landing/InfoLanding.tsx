import { defineComponent } from "vue";
import { IonPage } from "@ionic/vue";
import InfoLanding from "@/modules/public/info/pages/landing/InfoLanding.vue";

export default defineComponent({
  name: "SettingsInfoLanding",
  setup() {
    const props = { module: "Settings" };
    return () => (
      <IonPage>
        <div>
          <InfoLanding {...props}></InfoLanding>
        </div>
      </IonPage>
    );
  },
});
