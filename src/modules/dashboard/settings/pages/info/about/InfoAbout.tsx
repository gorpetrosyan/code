import { defineComponent } from "vue";
import { IonPage } from "@ionic/vue";
import InfoAbout from "@/modules/public/info/pages/about/InfoAbout.vue";

export default defineComponent({
  name: "SettingsInfoLanding",
  setup() {
    const props = { module: "Settings" };
    return () => (
      <IonPage>
        <div>
          <InfoAbout {...props}></InfoAbout>
        </div>
      </IonPage>
    );
  },
});
