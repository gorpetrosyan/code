import { defineComponent } from "vue";
import { IonPage } from "@ionic/vue";
import InfoTerms from "@/modules/public/info/pages/terms/InfoTerms.vue";

export default defineComponent({
  name: "SettingsInfoLanding",
  setup() {
    const props = { module: "Settings" };
    return () => (
      <IonPage>
        <div>
          <InfoTerms {...props}></InfoTerms>
        </div>
      </IonPage>
    );
  },
});
