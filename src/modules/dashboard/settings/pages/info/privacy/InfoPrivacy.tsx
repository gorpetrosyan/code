import { defineComponent } from "vue";
import { IonPage } from "@ionic/vue";
import InfoPrivacy from "@/modules/public/info/pages/privacy/InfoPrivacy.vue";

export default defineComponent({
  name: "SettingsInfoLanding",
  setup() {
    const props = { module: "Settings" };
    return () => (
      <IonPage>
        <div>
          <InfoPrivacy {...props}></InfoPrivacy>
        </div>
      </IonPage>
    );
  },
});
