import { defineComponent, ref } from "vue";
import { useRouter } from "vue-router";
import { Plugins } from "@capacitor/core";
const { Device } = Plugins;
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonSearchbar,
  IonCol,
  IonRow,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonText,
  IonFooter,
  alertController,
  IonButton,
} from "@ionic/vue";
import { errorToaster, successToaster } from "@/common/services/notifications/toaster/toasterService";
import { infoToaster } from "@/common/services/notifications/toaster/toasterService/toasterService";
import { socialApi } from "@/common/services/api";
import moment from "moment";
export default defineComponent({
  name: "ActiveLogins",
  components: {
    IonPage,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonSearchbar,
    IonContent,
    IonCol,
    IonRow,
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCardContent,
    IonText,
    IonFooter,
    alertController,
    IonButton,
  },
  setup() {
    const toggled = ref(false);
    const router = useRouter();
    const deviceList = ref([
      {
        icon: require("@/assets/icons/monitor.png"),
        isInAir: true,
        deviceName: "Macbook Air",
        deviceType: "Android",
        loggedInOn: moment("2021-02-10T12:26:25.884Z").format("MMM Do YY"),
        deviceId: "",
      },
      {
        icon: require("@/assets/icons/mobile.png"),
        isInAir: false,
        deviceName: "Iphone X",
        deviceType: "Android",
        loggedInOn: moment("2021-02-10T12:26:25.884Z").format("MMM Do YY"),
        deviceId: "",
      },
      {
        icon: require("@/assets/icons/mobile.png"),
        isInAir: false,
        deviceName: "Galaxy 50",
        deviceType: "Android",
        loggedInOn: moment("2021-02-10T12:26:25.884Z").format("MMM Do YY"),
        deviceId: "htytfytd",
      },
    ]);
    const searchQueryResult = ref([]);
    /**
     * checkout if device Is Online
     * @param deviceId
     */
    async function checkoutCurrentDevice(deviceId: string): Promise<boolean> {
      return await Device.getInfo().then((res: any) => {
        return deviceId === res.uuid;
      });
    }

    /**
     * find out the device logo
     * @param deviceType
     */
    async function checkoutDeviceIcon(deviceType: string): Promise<any> {
      if (deviceType === "Mobile") {
        return require("@/assets/icons/mobile.png");
      }
      return require("@/assets/icons/monitor.png");
    }

    async function getUserSessionsList(): Promise<void> {
      socialApi
        .getUserDeviceSessions()
        .then((results: any) => {
          results.forEach((device: any) => {
            Promise.all([checkoutDeviceIcon(device.deviceType), checkoutCurrentDevice(device.deviceId)]).then(
              (res: any) => {
                deviceList.value.push({
                  icon: res[0],
                  isInAir: res[1],
                  deviceName: device.deviceName,
                  deviceType: device.platform,
                  loggedInOn: moment(device.loggedInOn).format("MMM Do YY"),
                  deviceId: device.deviceId,
                });
              },
            );
          });
        })
        .catch((e: any) => {
          errorToaster(e.message);
        });
    }
    getUserSessionsList();

    /**
     * search bar
     */
    function searchToggled() {
      toggled.value = !toggled.value;
      searchQueryResult.value = [];
    }
    /**
     * redirect to relevant page
     */
    function redirectToURL(): void {
      router.push({
        name: "SettingsAccountLanding",
      });
    }

    /**
     * remove all sessions devices
     */
    async function removeAllSessions(): Promise<void> {
      const alert = await alertController.create({
        header: "Attention!",
        message: "<strong>You are going to delete your other sessions histories</strong>!!!",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            cssClass: "secondary",
            handler: (e: any) => {
              infoToaster("Removing cancelled successfully");
            },
          },
          {
            text: "Okay",
            handler: () => {
              socialApi
                .deleteUserOtherSessions()
                .then((res: any) => {
                  const activeDevice: any = deviceList.value.find((el: any) => {
                    return !!el.isInAir;
                  });
                  deviceList.value = [activeDevice];
                  successToaster("All other sessions removed successfully");
                })
                .catch((e: any) => {
                  errorToaster(e.message);
                });
            },
          },
        ],
      });
      return alert.present();
    }

    /**
     * remove current device
     * @param deviceId
     * @param deviceName
     * @param index
     */
    async function removeClickedSession(deviceId: string, deviceName: string, index: number): Promise<void> {
      const alert = await alertController.create({
        header: "Attention!",
        message: "Yor are going to delete <strong>" + deviceName + "</strong> session!!!",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            cssClass: "secondary",
            handler: (e: any) => {
              infoToaster("Removing cancelled successfully");
            },
          },
          {
            text: "Okay",
            handler: () => {
              socialApi
                .deleteUserSelectedSession(deviceId)
                .then((res: any) => {
                  deviceList.value.splice(index, 1);
                  successToaster(`The ${deviceName} was successfully removed`);
                })
                .catch((e: any) => {
                  errorToaster(e.message);
                });
            },
          },
        ],
      });
      return alert.present();
    }

    /**
     * search for device
     * @param filter
     */
    function searchDeviceSession(filter: string) {
      const result: any = deviceList.value.filter((device: any) => {
        if (
          device.deviceName.search(new RegExp(filter, "i")) < 0 ||
          device.deviceType.search(new RegExp(filter, "i")) < 0 ||
          device.loggedInOn.search(new RegExp(filter, "i")) < 0
        ) {
          return device;
        }
      });
      console.log(filter, "qu");
      console.log(result, "res");
      searchQueryResult.value = result;
    }
    return {
      toggled,
      deviceList,
      searchToggled,
      redirectToURL,
      removeAllSessions,
      removeClickedSession,
      searchQueryResult,
      searchDeviceSession,
    };
  },
});
