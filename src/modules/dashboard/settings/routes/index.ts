import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    name: "SettingsLanding",
    component: () => import("@/modules/dashboard/settings/pages/landing/SettingsLanding.vue"),
  },

  // App Settings

  {
    path: "app",
    name: "SettingsAppLanding",
    component: () => import("@/modules/dashboard/settings/pages/app/landing/SettingsAppLanding.vue"),
  },

  // Account Settings

  {
    path: "account",
    name: "SettingsAccountLanding",
    component: () => import("@/modules/dashboard/settings/pages/account/landing/SettingsAccountLanding.vue"),
  },
  {
    path: "account/contact",
    name: "SettingsAccountContact",
    component: () => import("@/modules/dashboard/settings/pages/account/contact/SettingsAccountContact.vue"),
  },
  {
    path: "account/security",
    name: "SettingsAccountSecurity",
    component: () => import("@/modules/dashboard/settings/pages/account/security/SettingsAccountSecurity.vue"),
  },
  {
    path: "account/activeLogins",
    name: "SettingsActiveLogins",
    component: () => import("@/modules/dashboard/settings/pages/account/activeLogins/ActiveLogins.vue"),
  },

  // Support Settings

  {
    path: "support",
    name: "SettingsSupportLanding",
    component: () => import("@/modules/dashboard/settings/pages/support/landing/SettingsSupportLanding.vue"),
  },
  {
    path: "support/contact",
    name: "SettingsSupportContact",
    component: () => import("@/modules/dashboard/settings/pages/support/contact/SettingsSupportContact.vue"),
  },
  {
    path: "support/faq",
    name: "SettingsSupportFAQ",
    component: () => import("@/modules/dashboard/settings/pages/support/faq/SettingsSupportFAQ.vue"),
  },

  // Info

  {
    path: "info",
    name: "SettingsInfoLanding",
    component: () => import("@/modules/dashboard/settings/pages/info/landing/InfoLanding"),
  },
  {
    path: "info/about",
    name: "SettingsInfoAbout",
    component: () => import("@/modules/dashboard/settings/pages/info/about/InfoAbout"),
  },
  {
    path: "info/privacy",
    name: "SettingsInfoPrivacy",
    component: () => import("@/modules/dashboard/settings/pages/info/privacy/InfoPrivacy"),
  },
  {
    path: "info/terms",
    name: "SettingsInfoTerms",
    component: () => import("@/modules/dashboard/settings/pages/info/terms/InfoTerms"),
  },
];

export default routes;
