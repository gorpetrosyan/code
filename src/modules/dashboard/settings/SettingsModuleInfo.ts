import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const SettingsModuleInfo = {
  [moduleNames.SETTINGS]: {
    path: import("@/modules/dashboard/settings/SettingsModule"),
    routes,
    state: {},
  },
};

export { SettingsModuleInfo };
