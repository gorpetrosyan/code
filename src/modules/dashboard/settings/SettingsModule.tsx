import { defineComponent } from "vue";
import { IonRouterOutlet } from "@ionic/vue";

export default defineComponent({
  name: "SettingsModule",
  setup() {
    return () => (
      <div>
        <IonRouterOutlet></IonRouterOutlet>
      </div>
    );
  },
});
