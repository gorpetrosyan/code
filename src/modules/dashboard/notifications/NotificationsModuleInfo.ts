import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const NotificationsModuleInfo = {
  [moduleNames.NOTIFICATIONS]: {
    path: import("@/modules/dashboard/notifications/NotificationsModule"),
    routes,
    state: {},
  },
};

export { NotificationsModuleInfo };
