import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    name: "NoficationsList",
    component: () => import("@/modules/dashboard/notifications/pages/list/NoficationsList.vue"),
  },
];

export default routes;
