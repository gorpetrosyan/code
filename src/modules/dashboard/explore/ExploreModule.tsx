import { defineComponent } from "vue";
import { IonRouterOutlet } from "@ionic/vue";

export default defineComponent({
  name: "ExploreModule",
  setup() {
    return () => (
      <div>
        <IonRouterOutlet></IonRouterOutlet>
      </div>
    );
  },
});
