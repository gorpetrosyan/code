import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    name: "ExploreLanding",
    component: () => import("@/modules/dashboard/explore/pages/landing/ExploreLanding.vue"),
  },
  {
    path: "search",
    name: "ExploreSearch",
    component: () => import("@/modules/dashboard/explore/pages/search/ExploreSearch.vue"),
  },
  {
    path: "invite",
    name: "InviteFriends",
    component: () => import("@/modules/dashboard/explore/pages/invite-friends/InviteFriends.vue"),
  },
];

export default routes;
