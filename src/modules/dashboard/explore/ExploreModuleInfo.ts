import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const ExploreModuleInfo = {
  [moduleNames.EXPLORE]: {
    path: import("@/modules/dashboard/explore/ExploreModule"),
    routes,
    state: {},
  },
};

export { ExploreModuleInfo };
