import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const ProfilesModuleInfo = {
  [moduleNames.PROFILES]: {
    path: import("@/modules/dashboard/profiles/ProfilesModule"),
    routes,
    state: {},
  },
};

export { ProfilesModuleInfo };
