import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [

  //  Shared Lists
  {
    path: "followers/:id",
    name: "ProfileViewFollowers",
    component: () => import("@/modules/dashboard/profiles/pages/shared/followers-list/FollowersList.vue"),
  },
  {
    path: "friends/:id",
    name: "ProfileViewFriends",
    component: () => import("@/modules/dashboard/profiles/pages/shared/friends-list/FriendsList.vue"),
  },
  {
    path: "players/:clubId/:teamId",
    name: "ProfileViewPlayersList",
    component: () => import("@/modules/dashboard/profiles/pages/shared/players-list/PlayersList.vue"),
  },
  {
    path: "notice/create",
    name: "NoticeCreate",
    component: () => import("@/modules/dashboard/profiles/pages/shared/noticeboard/create/NoticeCreate.vue"),
  },
  {
    path: "notice/edit/:noticeId",
    name: "NoticeEdit",
    component: () => import("@/modules/dashboard/profiles/pages/shared/noticeboard/create/NoticeCreate.vue"),
  },
  {
    path: "notice/list/:id",
    name: "NoticeList",
    component: () => import("@/modules/dashboard/profiles/pages/shared/noticeboard/list/NoticeList.vue"),
  },
  {
    path: "notice/view/:noticeId",
    name: "NoticeView",
    component: () => import("@/modules/dashboard/profiles/pages/shared/noticeboard/view/NoticeView.vue"),
  },

  //  Clubs
  {
    path: "clubs/view/:id",
    name: "ProfileViewClub",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/view/ProfileClubView.vue"),
  },
  {
    path: "clubs/:id/members",
    name: "ProfileViewClubMembers",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/members/ProfileClubMembers.vue"),
  },
  {
    path: "clubs/:id/add/members",
    name: "ClubAddMembers",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/add-members/ClubAddMembers.vue"),
  },
  {
    path: "clubs/create/",
    name: "CreateClub",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/create/ClubCreate.vue"),
  },
  {
    path: "clubs/edit/:id",
    name: "EditClub",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/create/ClubCreate.vue"),
  },

  //  Teams
  {
    path: "clubs/teams/create/:clubId",
    name: "CreateTeam",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/teams/create/ClubCreateTeam.vue"),
  },
  {
    path: "clubs/teams/list/:id",
    name: "ListTeam",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/teams/list/ClubTeamsList.vue"),
  },
  {
    path: "team/view/:id",
    name: "ProfileViewTeam",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/teams/view/ProfileTeamView.vue"),
  },
  {
    path: "team/view/:clubId/:teamId/members",
    name: "ProfileViewTeamMembers",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/teams/members/ProfileTeamMembers.vue"),
  },
  {
    path: "team/view/:clubId/:teamId/add/members",
    name: "ProfileTeamAddMembers",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/teams/add-members/TeamAddMembers.vue"),
  },
  {
    path: "clubs/teams/edit/:clubId/:teamId",
    name: "ClubEditTeam",
    component: () => import("@/modules/dashboard/profiles/pages/clubs/teams/create/ClubCreateTeam.vue"),
  },

  //  Coaches
  {
    path: "coaches/view/:id",
    name: "ProfileViewCoach",
    component: () => import("@/modules/dashboard/profiles/pages/coaches/view/ProfileCoachView.vue"),
  },
  {
    path: "coaches/create",
    name: "CreateCoach",
    component: () => import("@/modules/dashboard/profiles/pages/coaches/create/CoachCreate.vue"),
  },
  {
    path: "coaches/edit/:id",
    name: "EditCoach",
    component: () => import("@/modules/dashboard/profiles/pages/coaches/create/CoachCreate.vue"),
  },

  //  Players
  {
    path: "players/view/:id",
    name: "ProfileViewPlayer",
    component: () => import("@/modules/dashboard/profiles/pages/players/view/ProfilePlayerView.vue"),
  },
  {
    path: "players/preview/info/:id",
    name: "PreviewPlayerInfo",
    component: () => import("@/modules/dashboard/profiles/pages/players/preview/info/PlayerPreiewInfo.vue"),
  },
  {
    path: "players/preview/sport/:id/:userId",
    name: "PreviewPlayerSport",
    component: () => import("@/modules/dashboard/profiles/pages/players/preview/sport/PlayerPreiewSport.vue"),
  },
  {
    path: "players/create/about",
    name: "CreatePlayerAbout",
    component: () => import("@/modules/dashboard/profiles/pages/players/create/about/PlayerCreateAbout.vue"),
  },
  {
    path: "players/create/photo",
    name: "CreatePlayerPhoto",
    component: () => import("@/modules/dashboard/profiles/pages/players/create/photo/PlayerCreatePhoto.vue"),
  },
  {
    path: "players/create/sports",
    name: "CreatePlayerSports",
    component: () => import("@/modules/dashboard/profiles/pages/players/create/sports/PlayerCreateSports.vue"),
  },

  //  Venues
  // {
  //   path: "veunes/view/:id",
  //   name: "ProfileViewVenue",
  //   component: () => import("@/modules/dashboard/profiles/pages/venues/view/ProfileVenueView.vue"),
  // },
  // {
  //   path: "veunes/create/about",
  //   name: "CreateVenueAbout",
  //   component: () => import("@/modules/dashboard/profiles/pages/venues/view/ProfileVenueView.vue"),
  // },
  // {
  //   path: "veunes/create/contact",
  //   name: "CreateVenueContact",
  //   component: () => import("@/modules/dashboard/profiles/pages/venues/create/contact/VenueCreateContact.vue"),
  // },
  // {
  //   path: "veunes/create/features",
  //   name: "CreateVenueFeatures",
  //   component: () => import("@/modules/dashboard/profiles/pages/venues/create/features/VenueCreateFeatures.vue"),
  // },
  // {
  //   path: "veunes/create/sports",
  //   name: "CreateVenueSports",
  //   component: () => import("@/modules/dashboard/profiles/pages/venues/create/VenueCreateSports.vue"),
  // },
];

export default routes;
