import {
  IonPage,
  IonContent,
  IonRow,
  IonCol,
  IonImg,
  IonAvatar,
  IonLabel,
  IonText,
  IonSlide,
  IonSlides,
  IonChip,
  IonButton,
} from "@ionic/vue";

import { defineComponent } from "vue";
import TimelineStream from "@/modules/dashboard/profiles/components/timeline/TimelineStream.vue";
import { profileApi } from "@/common/services/api/profile";
import { useLocalStorage } from "@/common/services/storage/localStorageService";
import RedirectBackComponent from "@/widgets/components/pages/RedirectBackComponent.vue";
import ClubViewIconComponent from "@/widgets/components/profiles/ClubViewIconComponent.vue";
import ClubComponent from "@/widgets/components/profiles/ClubComponent.vue";
import ClubStatsComponent from "@/widgets/components/profiles/ClubStatsComponent.vue";
import ClubDescriptionComponent from "@/widgets/components/profiles/ClubDescriptionComponent.vue";
import ClubSportComponent from "@/widgets/components/profiles/ClubSportComponent.vue";
import ClubActionsComponent from "@/widgets/components/profiles/ClubActionsComponent.vue";
import ClubPhotoCountComponent from "@/widgets/components/profiles/ClubPhotoCountComponent.vue";
import ClubTeamComponent from "@/widgets/components/profiles/ClubTeamComponent.vue";
import ClubMembersComponent from "@/widgets/components/profiles/ClubMembersComponent.vue";
import ClubFollowersComponent from "@/widgets/components/profiles/ClubFollowersComponent.vue";

export default defineComponent({
  name: "ProfileClubView",
  components: {
    IonContent,
    IonPage,
    IonRow,
    IonCol,
    IonImg,
    IonAvatar,
    IonLabel,
    IonText,
    IonSlide,
    IonSlides,
    IonChip,
    IonButton,
    TimelineStream,
    RedirectBackComponent,
    ClubViewIconComponent,
    ClubComponent,
    ClubStatsComponent,
    ClubDescriptionComponent,
    ClubSportComponent,
    ClubActionsComponent,
    ClubPhotoCountComponent,
    ClubTeamComponent,
    ClubMembersComponent,
    ClubFollowersComponent,
  },
  data() {
    return {
      clubId: Number(this.$route.params.id),
      profileId: useLocalStorage("profile_id").value,
      sportData: [] as Array<any>,
      queryParams: {},
      basicInfo: {} as any,
      playgrounds: [],
      clubSports: "",
      members: []
    };
  },
  mounted() {
    this.getSports();
    this.getBasicInfo();
    this.getPosts();
    this.getMembers();
  },
  methods: {
    getSports: async function () {
      const sportData = await profileApi.getClubSports(this.clubId);
      this.sportData = sportData
      const sportsList = [];
      for (const sport of sportData) {
        sportsList.push(sport.sportName);
      }
      console.log('sports', sportsList)
      this.clubSports = sportsList.toString().replace(/,/g, ", ");
      this.queryParams = {
        id: this.sportData[0]?.sportId,
        userId: this.clubId,
      };
    },
    getBasicInfo: async function () {
      this.basicInfo = await profileApi.getBasicClubInfo(this.clubId);
    },
    getPosts: async function () {
      const data = await profileApi.getPosts(this.clubId);
      this.playgrounds = data.data;
    },
    getMembers: async function () {
      const res = await profileApi.getClubMembers(this.clubId);
      this.members = res.data;
    },
  },
});
