import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonItem,
  IonList,
  IonLabel,
  IonSearchbar,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonAvatar,
} from "@ionic/vue";
import { ref, reactive, onMounted } from "vue";
import { defineComponent } from "vue";
import { profileApi } from "@/common/services/api/profile";
import { useRoute, useRouter } from "vue-router";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";
export default defineComponent({
  name: "ClubTeamsList",
  components: {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonItem,
    IonList,
    IonLabel,
    IonSearchbar,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
    IonAvatar,
  },
  setup() {
    const toggled = ref(false);
    const friends = ref([]);
    const isDisabled = ref(false);
    let metaData = reactive({
      limit: 20,
      offset: 0,
      totalPages: 3,
    });
    const page = ref(1);
    const route = useRoute();
    const router = useRouter();
    const paramId = ref(Number(route.params.id));

    /**
     * get friends list
     * @param event
     */
    async function getFriendsList(event: any) {
      await profileApi
        .getTeamList(paramId.value, metaData.limit, metaData.offset)
        .then((data: any) => {
          friends.value = data.data;
          metaData = data.data.metaData;
          if (event !== "other" && event) {
            event.target.complete();
          }
        })
        .catch((e: any) => {
          errorToaster(e.message ?? e.errorMessage);
        });
    }

    /**
     * load more data
     * @param event
     */
    async function loadData(event: any) {
      metaData.limit = metaData.limit + 20;
      await getFriendsList(event);
      if (page.value === metaData.totalPages) {
        isDisabled.value = true;
        event.target.disabled = true;
      }
    }

    /**
     * toggle search bar
     */
    function searchToggled() {
      toggled.value = !toggled.value;
    }

    /**
     * redirect to url
     * @param routName
     * @param paramsList
     */
    function redirectToPage(routName: string, paramsList: any): void {
      router.push({
        name: routName,
        params: paramsList,
      });
    }
    onMounted(async () => {
      await getFriendsList("other");
    });
    return {
      loadData,
      searchToggled,
      toggled,
      friends,
      isDisabled,
      paramId,
      router,
      redirectToPage,
    };
  },
});
