import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonItem,
  IonList,
  IonLabel,
  IonSearchbar,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonAvatar,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
} from "@ionic/vue";
import { defineComponent, ref, reactive, onMounted } from "vue";
import { socialApi } from "@/common/services/api/social";
import { profileApi } from "@/common/services/api/profile";
import { useRouter } from "vue-router";
import { defaultUserData } from "@/config/properties/user/userData";

export default defineComponent({
  name: "FriendsList",
  components: {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonItem,
    IonList,
    IonLabel,
    IonSearchbar,
    IonRow,
    IonSegment,
    IonSegmentButton,
    IonAvatar,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
  },
  setup() {
    const router = useRouter();
    const toggled = ref(false);
    const defaultAvatar = defaultUserData.avatar;
    const friends = ref([]);
    const isDisabled = ref(false);
    let metaData = reactive({
      limit: 20,
      offset: 0,
      totalPages: 3,
    });
    const page = ref(1);
    const segment = ref("friends");
    const segmentList: Array<object> = [
      {
        value: "friends",
        name: "Friends",
      },
      {
        value: "requests", 
        name: "Requests",
      },
      {
        value: "requested",
        name: "Requested",
      },
    ];
    const filteredFriends: any = ref([]);
    /**
     * get friends list
     * @param event
     */
    async function getFriendsList(event: any): Promise<void> {
      let res: any = {};
      if (segment.value === "friends") {
        res = await socialApi.getFriendsList(metaData.limit, metaData.offset);
      } else if (segment.value === "requests") {
        res = await profileApi.getFriendRequests();
      } else if (segment.value === "requested") {
        res = await profileApi.getFriendRequestsSent();
      }
      friends.value = filteredFriends.value = res.data;
      metaData = res.metaData;
      if (event !== "other" && event) {
        event.target.complete();
      }
    }

    async function gotoProfile(item: any) {
      if (item.type == "U") {
        router.push({ name: "ProfileViewPlayer", params: { id: item?.userId } });
      }
      if (item.type == "V") {
        router.push({ name: "ProfileViewVenue", params: { id: item?.userId } });
      }
      if (item.type == "C") {
        router.push({ name: "ProfileViewCoach", params: { id: item?.userId } });
      }
      if (item.type == "T") {
        router.push({ name: "ProfileViewClub", params: { id: item?.userId } });
      }
    }

    /**
     * filter friends list
     * @param str
     */
    function filterFriends(str: string) {
      filteredFriends.value = [];
      friends.value.forEach((data: any) => {
        if (data.user) {
          if (data.user["fullName"].toLowerCase().startsWith(str.toLowerCase())) {
            filteredFriends.value.push(data);
          }
        } else {
          if (data.fullName.toLowerCase().startsWith(str.toLowerCase())) {
            filteredFriends.value.push(data);
          }
        }
      });
    }
    /**
     * load more data
     * @param event
     */
    async function loadData(event: any) {
      metaData.limit += 20;
      await getFriendsList(event);
      if (page.value === metaData.totalPages) {
        isDisabled.value = true;
        event.target.disabled = true;
      }
    }
    /**
     * segment change
     * @param segmentA
     */
    async function segmentChanged(segmentA: string) {
      segment.value = segmentA;
      await getFriendsList("other");
    }
    /**
     * get kind
     * @param type
     */
    function getKind(type: string) {
      return profileApi.getProfileKind(type);
    }
    /**
     * searchbar toggle
     */
    function searchToggled() {
      toggled.value = !toggled.value;
    }
    onMounted(async () => {
      await getFriendsList("other");
    });
    return {
      router,
      toggled,
      defaultAvatar,
      isDisabled,
      segment,
      segmentList,
      filteredFriends,
      searchToggled,
      getKind,
      segmentChanged,
      loadData,
      filterFriends,
      gotoProfile,
    };
  },
});
