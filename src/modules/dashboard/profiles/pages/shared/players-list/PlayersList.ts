import {
  IonContent,
  IonPage,
  IonTitle,
  IonToolbar,
  IonItem,
  IonList,
  IonLabel,
  IonSearchbar,
  IonAvatar,
  IonButton,
  IonInfiniteScrollContent,
  IonInfiniteScroll,
} from "@ionic/vue";

import { defineComponent, ref, reactive, onMounted } from "vue";
import { profileApi } from "@/common/services/api/profile";
import { useRouter, useRoute } from "vue-router";
import { defaultUserData } from "@/config/properties/user/userData";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";
export default defineComponent({
  name: "PlayersList",
  components: {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonItem,
    IonList,
    IonLabel,
    IonSearchbar,
    IonAvatar,
    IonButton,
    IonInfiniteScrollContent,
    IonInfiniteScroll,
  },
  setup() {
    const toggled = ref(false);
    const friends = ref([]);
    const isDisabled = ref(false);
    const router = useRouter();
    const route = useRoute();
    const clubId = ref(Number(route.params.clubId));
    const teamId = ref(Number(route.params.teamId));
    const userAvatar = defaultUserData.avatar;
    let metaData = reactive({
      limit: 20,
      offset: 0,
      totalPages: 3,
    });
    const page = ref(1);

    /**
     * get players
     * @param clubId
     * @param teamId
     * @param event
     */
    async function getPlayers(clubId: number, teamId: number, event: any) {
      profileApi
        .getTeamMembers(teamId, 20, 0)
        .then((data: any) => {
          friends.value = data.data;
          metaData = data.metaData;
        })
        .catch((e: any) => {
          errorToaster(e.message ?? e.errorMessage);
        });
      if (event !== "other" && event) {
        event.target.complete();
      }
    }

    /**
     * load more data
     * @param event
     */
    async function loadData(event: any) {
      metaData.limit += 20;
      await getPlayers(clubId.value, teamId.value, event);
      if (page.value === metaData.totalPages) {
        isDisabled.value = true;
        event.target.disabled = true;
      }
    }

    /**
     * search icon toggle
     */
    function searchToggled() {
      toggled.value = !toggled.value;
    }
    onMounted(async () => {
      await getPlayers(clubId.value, teamId.value, "other");
    });
    async function gotoProfile(item: any) {
      if (item.type == "U") {
        router.push({ name: "ProfileViewPlayer", params: { id: item?.userId } });
      }
      if (item.type == "V") {
        router.push({ name: "ProfileViewVenue", params: { id: item?.userId } });
      }
      if (item.type == "C") {
        router.push({ name: "ProfileViewCoach", params: { id: item?.userId } });
      }
      if (item.type == "T") {
        router.push({ name: "ProfileViewClub", params: { id: item?.userId } });
      }
    }
    return {
      toggled,
      friends,
      isDisabled,
      router,
      userAvatar,
      loadData,
      searchToggled,
      gotoProfile,
    };
  },
});
