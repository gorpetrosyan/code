import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonItem,
  IonList,
  IonLabel,
  IonSearchbar,
  IonAvatar,
  IonInfiniteScrollContent,
  IonInfiniteScroll,
} from "@ionic/vue";
import { defineComponent, ref, reactive, onMounted } from "vue";
import { profileApi } from "@/common/services/api/profile";
import { useRoute, useRouter } from "vue-router";
import { defaultUserData } from "@/config/properties/user/userData";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";
export default defineComponent({
  name: "FollowersList",
  components: {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonItem,
    IonList,
    IonLabel,
    IonSearchbar,
    IonAvatar,
    IonInfiniteScrollContent,
    IonInfiniteScroll,
  },
  setup() {
    const router = useRouter();
    const route = useRoute();
    const toggled = ref(false);
    const defaultAvatar = defaultUserData.avatar;
    const followers = ref([]);
    const isDisabled = ref(false);
    let metaData = reactive({
      limit: 20,
      offset: 0,
      totalPages: 3,
    });
    const page = ref(1);
    const userId = ref(Number(route.params.id));

    async function getFollowersList(id: number, event: any) {
      profileApi
        .getFollowersList(id)
        .then((res: any) => {
          followers.value = res.data;
          metaData = res.metaData;
          if (event !== "other" && event) {
            event.target.complete();
          }
        })
        .catch((e: any) => {
          errorToaster(e.message ?? e.errorMessage);
        });
    }
    /**
     * load more data
     * @param event
     */
    async function loadData(event: any) {
      metaData.limit += 20;
      await getFollowersList(userId.value, event);
      if (page.value === metaData.totalPages) {
        isDisabled.value = true;
        event.target.disabled = true;
      }
    }
    function searchToggled() {
      toggled.value = !toggled.value;
    }
    onMounted(async () => {
      await getFollowersList(userId.value, "other");
    });
    async function gotoProfile(item: any) {
      if (item.type == "U") {
        router.push({ name: "ProfileViewPlayer", params: { id: item?.userId } });
      }
      if (item.type == "V") {
        router.push({ name: "ProfileViewVenue", params: { id: item?.userId } });
      }
      if (item.type == "C") {
        router.push({ name: "ProfileViewCoach", params: { id: item?.userId } });
      }
      if (item.type == "T") {
        router.push({ name: "ProfileViewClub", params: { id: item?.userId } });
      }
    }
    return {
      router,
      toggled,
      defaultAvatar,
      followers,
      isDisabled,
      loadData,
      searchToggled,
      gotoProfile,
    };
  },
});
