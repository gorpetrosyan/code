import { defineComponent } from "vue";
import { IonRouterOutlet } from "@ionic/vue";

export default defineComponent({
  name: "ProfilesModule",
  setup() {
    return () => (
      <div>
        <IonRouterOutlet></IonRouterOutlet>
      </div>
    );
  },
});
