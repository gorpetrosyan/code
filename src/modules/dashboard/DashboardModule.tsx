import { defineComponent } from "vue";

import TheSideMenu from "./shared/layout/side-menu/TheSideMenu.vue";

export default defineComponent({
  name: "DashboardModule",
  setup() {
    return () => (
      <div>
        <TheSideMenu></TheSideMenu>
      </div>
    );
  },
});
