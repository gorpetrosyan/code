import moduleNames from "@/config/modules/moduleTypes";

const DashboardModuleInfo = {
  [moduleNames.DASHBOARD]: {
    path: import("@/modules/dashboard/DashboardModule"),
    routes: [],
    state: {},
  },
};

export { DashboardModuleInfo };
