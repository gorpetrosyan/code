import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const MessagesModuleInfo = {
  [moduleNames.MESSAGES]: {
    path: import("@/modules/dashboard/messages/MessagesModule"),
    routes,
    state: {},
  },
};

export { MessagesModuleInfo };
