import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    redirect: "messages/view/",
  },
  {
    path: "view/:route?/:path?",
    name: "ChatFrame",
    component: () => import("@/modules/dashboard/messages/pages/ChatFrame/ChatFrame.vue"),
  },
];

export default routes;
