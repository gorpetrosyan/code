import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "",
    redirect: "create",
  },
  {
    path: "create",
    name: "CreateBasicEvent",
    component: () => import("@/modules/dashboard/events/pages/basic/EventsCreateBasic.vue"),
  },
  {
    path: "edit/:type/:id",
    name: "EditEvent",
    component: () => import("@/modules/dashboard/events/pages/basic/EventsEditBasic.vue"),
  },
  {
    path: "create/advanced",
    name: "CreateAdvancedEvent",
    component: () => import("@/modules/dashboard/events/pages/advanced/EventsAdvanced.vue"),
  },
  {
    path: "edit/advanced/:type/:id",
    name: "EditAdvancedEvent",
    component: () => import("@/modules/dashboard/events/pages/advanced/EventsAdvanced.vue"),
  },
  {
    path: "view/:type/:id",
    name: "ViewEvent",
    component: () => import("@/modules/dashboard/events/pages/view/details/EventsViewDetails.vue"),
  },
  {
    path: "surveys/answer/:id",
    name: "SurveyAnswerForm",
    component: () => import("@/modules/dashboard/events/pages/surveys/answer-survey/SurveyAnswerForm.vue"),
  },
  {
    path: "surveys/respondents/:id",
    name: "SurveyViewRespondents",
    component: () => import("@/modules/dashboard/events/pages/surveys/view-respondents/SurveyViewRespondents.vue"),
  },
  {
    path: "surveys/view-answers/:id/:userId",
    name: "SurveyViewAnswers",
    component: () => import("@/modules/dashboard/events/pages/surveys/view-answers/SurveyViewAnswers.vue"),
  },
  {
    path: "players/:type/:id",
    name: "PlayerOfEvents",
    component: () => import("@/modules/dashboard/events/pages/view/event-players/EventPlayers.vue"),
  },
  {
    path: "invites/:type/:id",
    name: "EventInvites",
    component: () => import("@/modules/dashboard/events/pages/view/invites/EventInvites.vue"),
  },
  {
    path: "invite-more/:type/:id",
    name: "EventInviteMore",
    component: () => import("@/modules/dashboard/events/pages/view/invites/EventInviteMore.vue"),
  },
  {
    path: "challenge/view/:id",
    name: "ChallengeViewDetails",
    component: () => import("@/modules/dashboard/events/pages/view/challange/ChallengeViewDetails.vue"),
  },
];

export default routes;
