import routes from "./routes";
import moduleNames from "@/config/modules/moduleTypes";

const EventsModuleInfo = {
  [moduleNames.EVENTS]: {
    path: import("@/modules/dashboard/events/EventsModule"),
    routes,
    state: {},
  },
};

export { EventsModuleInfo };
