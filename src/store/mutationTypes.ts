import { MutationTypes as userTypes } from "./modules/user/mutationTypes";
import { MutationTypes as rootMTypes } from "./modules/root/mutationTypes";
import { MutationTypes as profileMTypes } from "./modules/profile/mutationTypes";
import { MutationTypes as notificationMTypes } from "./modules/notification/mutationTypes";

export const AllMutationTypes = { ...userTypes, ...rootMTypes, ...profileMTypes, ...notificationMTypes };
