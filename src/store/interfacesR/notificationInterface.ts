import { MutationTypes as NotificationMTypes } from "@/store/modules/notification/mutationTypes";
import { ActionTypes as NotificationATypes } from "@/store/modules/notification/actionTypes";
import { ActionContext } from "vuex";
import { RootState } from "@/store/interfaces";

export interface NotificationStateTypes {
  notifications: Array<object> | null;
  unReadNotifications: number;
  unSeenNotifications: number;
  allowNotification: boolean;
}

export interface NotificationGettersTypes {
  getNotificationData(state: NotificationStateTypes, notificationId: string): any;
  getAllNotificationsData(state: NotificationStateTypes): Array<object> | null;
  getUnReadNotifications(state: NotificationStateTypes): number;
  getUnSeenNotifications(state: NotificationStateTypes): number;
  getNotificationAllowState(state: NotificationStateTypes): boolean;
}

export type NotificationMutationsTypes<S = NotificationStateTypes> = {
  [NotificationMTypes.SET_NOTIFICATIONS](state: S, payload: Array<object> | null): void;
  [NotificationMTypes.SET_UN_READ_NOTIFICATIONS](state: S, payload: number): void;
  [NotificationMTypes.SET_READ_NOTIFICATION](state: S, payload: string): void;
  [NotificationMTypes.SET_UN_SEEN_NOTIFICATIONS](state: S, payload: number): void;
  [NotificationMTypes.SET_SEEN_NOTIFICATION](state: S, payload: string): void;
  [NotificationMTypes.REMOVE_NOTIFICATION](state: S, payload: string): void;
  [NotificationMTypes.SET_NOTIFICATION_PERMISSION](state: S, payload: boolean): void;
};

export type AugmentedActionContext = {
  commit<K extends keyof NotificationMutationsTypes>(
    key: K,
    payload: Parameters<NotificationMutationsTypes[K]>[1],
  ): ReturnType<NotificationMutationsTypes[K]>;
} & Omit<ActionContext<NotificationStateTypes, RootState>, "commit">;

export interface NotificationActionsTypes {
  [NotificationATypes.GET_NOTIFICATIONS]({ commit }: AugmentedActionContext, payload: Array<object> | null): void;
  [NotificationATypes.GET_UN_READ_NOTIFICATIONS]({ commit }: AugmentedActionContext, payload: number): void;
  [NotificationATypes.GET_UN_SEEN_NOTIFICATIONS]({ commit }: AugmentedActionContext, payload: number): void;
  [NotificationATypes.REMOVE_NOTIFICATION]({ commit }: AugmentedActionContext, payload: string): void;
  [NotificationATypes.SEEN_NOTIFICATION]({ commit }: AugmentedActionContext, payload: string): void;
  [NotificationATypes.READ_NOTIFICATION]({ commit }: AugmentedActionContext, payload: string): void;
  [NotificationATypes.STATUS_NOTIFICATION]({ commit }: AugmentedActionContext, payload: string): void;
  [NotificationATypes.GET_NOTIFICATION_PERMISSION]({ commit }: AugmentedActionContext, payload: boolean): void;
}
