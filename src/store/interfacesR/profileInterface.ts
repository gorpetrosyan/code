import { MutationTypes as ProfileMTypes } from "@/store/modules/profile/mutationTypes";
import { ActionTypes as ProfileATypes } from "@/store/modules/profile/actionTypes";
import { ActionContext } from "vuex";
import { RootState } from "@/store/interfaces";

export interface ProfileStateTypes {
  sports: object | null;
  profile: Array<object> | null;
  clubStates?: object | null;
  rootDispatch?: boolean;
}

export interface ProfileGettersTypes {
  getProfileData(state: ProfileStateTypes): Array<object> | null;
  getSportData(state: ProfileStateTypes): object | null;
  getClubStateData(state: ProfileStateTypes): object | null;
  getProfileDataById(state: ProfileStateTypes, id: number): object | null;
}

export type ProfileMutationsTypes<S = ProfileStateTypes> = {
  [ProfileMTypes.SET_PROFILE](state: S, payload: Array<object> | null): void;
  [ProfileMTypes.RESET_PROFILE](state: S): void;
  [ProfileMTypes.SET_SPORTS](state: S, payload: object | null): void;
  [ProfileMTypes.RESET_SPORTS](state: S): void;
  [ProfileMTypes.SET_CLUB_STATES](state: S, payload: object | null): void;
  [ProfileMTypes.RESET_CLUB_STATES](state: S): void;
};

export type AugmentedActionContext = {
  commit<K extends keyof ProfileMutationsTypes>(
    key: K,
    payload: Parameters<ProfileMutationsTypes[K]>[1],
  ): ReturnType<ProfileMutationsTypes[K]>;
} & Omit<ActionContext<ProfileStateTypes, RootState>, "commit">;

export interface ProfileActionsTypes {
  [ProfileATypes.GET_PROFILE]({ commit }: AugmentedActionContext, payload: Array<object> | null): void;
  [ProfileATypes.GET_SPORTS]({ commit }: AugmentedActionContext, payload: object | null): void;
  [ProfileATypes.GET_CLUB_STATES]({ commit }: AugmentedActionContext, payload: object | null): void;
}
