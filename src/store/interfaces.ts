import { ActionContext } from "vuex";
import { MutationTypes as UserMTypes } from "./modules/user/mutationTypes";
import { ActionTypes as UserATypes } from "./modules/user/actionTypes";
import { MutationTypes as RootMTypes } from "./modules/root/mutationTypes";
import { ActionTypes as RootATypes } from "./modules/root/actionTypes";
import { ProfileActionsTypes, ProfileGettersTypes, ProfileStateTypes } from "./interfacesR/profileInterface";
import {
  NotificationActionsTypes,
  NotificationGettersTypes,
  NotificationStateTypes
} from "./interfacesR/notificationInterface";

export interface RootState {
  root: boolean;
  version: string;
}

export interface MergedState extends RootState {
  userModule: UserStateTypes;
  profileModule: ProfileStateTypes;
  notificationModule: NotificationStateTypes;
}

export interface RootGettersTypes {
  getVersion(state: RootState): string;
}

export type RootMutationsTypes<S = RootState> = {
  [RootMTypes.UPDATE_VERSION](state: S, payload: string): void;
};

type AugmentedActionContextRoot = {
  commit<K extends keyof RootMutationsTypes>(
    key: K,
    payload: Parameters<RootMutationsTypes[K]>[1],
  ): ReturnType<RootMutationsTypes[K]>;
} & Omit<ActionContext<RootState, RootState>, "commit">;

export interface RootActionsTypes {
  [RootATypes.UPDATE_VERSION]({ commit }: AugmentedActionContextRoot, payload: string): void;
}

/*********************** User MODULE TYPES  ***********************/
export interface UserStateTypes {
  user?: object | null;
  rootDispatch?: boolean;
}

export interface UserGettersTypes {
  getUserData(state: UserStateTypes): object | null;
}

export type UserMutationsTypes<S = UserStateTypes> = {
  [UserMTypes.SET_USER](state: S, payload: object | null): void;
  [UserMTypes.RESET_USER](state: S): void;
};

export type AugmentedActionContext = {
  commit<K extends keyof UserMutationsTypes>(
    key: K,
    payload: Parameters<UserMutationsTypes[K]>[1],
  ): ReturnType<UserMutationsTypes[K]>;
} & Omit<ActionContext<UserStateTypes, RootState>, "commit">;

export interface UserActionsTypes {
  [UserATypes.GET_USER]({ commit }: AugmentedActionContext, payload: object | null): void;
}
export interface StoreActions extends RootActionsTypes, ProfileActionsTypes, UserActionsTypes, NotificationActionsTypes {}
export interface StoreGetters extends RootGettersTypes, UserGettersTypes, ProfileGettersTypes, NotificationGettersTypes {}
