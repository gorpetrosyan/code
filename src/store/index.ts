import { createStore } from "vuex";
import { RootState } from "@/store/interfaces";
import { RootStoreModuleTypes } from "./modules/root/types";
import { UserStoreModuleTypes } from "./modules/user/types";
import { ProfileStoreModuleTypes } from "./modules/profile/types";
import { NotificationStoreModuleTypes} from "./modules/notification/types";

import root from "./modules/root";

export const store = createStore<RootState>(root);

type StoreModules = {
  user: UserStoreModuleTypes;
  profile: ProfileStoreModuleTypes;
  root: RootStoreModuleTypes;
  notification: NotificationStoreModuleTypes;
};

export type Store = UserStoreModuleTypes<Pick<StoreModules, "user">> &
  RootStoreModuleTypes<Pick<StoreModules, "root">> &
  NotificationStoreModuleTypes<Pick<StoreModules, "notification">> &
  ProfileStoreModuleTypes<Pick<StoreModules, "profile">>;
