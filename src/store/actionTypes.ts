import { ActionTypes as userTypes } from "./modules/user/actionTypes";
import { ActionTypes as rootATypes } from "./modules/root/actionTypes";
import { ActionTypes as profileATypes } from "./modules/profile/actionTypes";
import { ActionTypes as notificationATypes } from "./modules/notification/actionTypes";

export const AllActionTypes = { ...userTypes, ...rootATypes, ...profileATypes, ...notificationATypes };
