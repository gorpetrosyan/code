import { MutationTree } from "vuex";
import { MutationTypes } from "./mutationTypes";
import { UserMutationsTypes, UserStateTypes } from "./../../interfaces";

export const mutations: MutationTree<UserStateTypes> & UserMutationsTypes = {
  [MutationTypes.SET_USER](state: UserStateTypes, payload: object | null) {
    state.user = payload;
  },
  [MutationTypes.RESET_USER](state: UserStateTypes) {
    state.user = null;
  },
};
