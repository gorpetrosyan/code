import { ActionTree } from "vuex";
import { ActionTypes } from "./actionTypes";
import { MutationTypes } from "./mutationTypes";
import { UserActionsTypes, UserStateTypes, RootState } from "@/store/interfaces";

export const actions: ActionTree<UserStateTypes, RootState> & UserActionsTypes = {
  [ActionTypes.GET_USER]({ commit }, payload: object | null): void {
    commit(MutationTypes.SET_USER, payload);
  },
  [ActionTypes.RESET_USER]({ commit }): void {
    commit(MutationTypes.RESET_USER);
  },
};
