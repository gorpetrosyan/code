export enum ActionTypes {
  GET_USER = "GET_USER",
  RESET_USER = "RESET_USER",
}
