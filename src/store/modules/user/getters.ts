import { GetterTree } from "vuex";
import { UserGettersTypes, UserStateTypes, RootState } from "./../../interfaces";

export const getters: GetterTree<UserStateTypes, RootState> & UserGettersTypes = {
  getUserData: (state: UserStateTypes) => {
    return state.user || null;
  },
};
