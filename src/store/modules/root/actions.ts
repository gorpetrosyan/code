import { ActionTree } from "vuex";
import { ActionTypes } from "./actionTypes";
import { MutationTypes } from "./mutationTypes";
import { RootActionsTypes, RootState } from "@/store/interfaces";

export const actions: ActionTree<RootState, RootState> & RootActionsTypes = {
  [ActionTypes.UPDATE_VERSION]({ commit }, payload: string) {
    commit(MutationTypes.UPDATE_VERSION, payload);
  },
};
