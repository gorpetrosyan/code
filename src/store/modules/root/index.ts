import { Module, ModuleTree } from "vuex";
import { RootState } from "@/store/interfaces";
import { getters } from "./getters";
import { actions } from "./actions";
import { mutations } from "./mutations";
import { state } from "./state";
import userModule from "../user";
import profileModule from "../profile";
import notificationModule from "../notification";

// Modules
const modules: ModuleTree<RootState> = {
  userModule,
  profileModule,
  notificationModule,
};

const root: Module<RootState, RootState> = {
  state,
  getters,
  mutations,
  actions,
  modules,
};

export default root;
