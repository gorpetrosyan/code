import { GetterTree } from "vuex";
import { RootGettersTypes, RootState } from "./../../interfaces";

export const getters: GetterTree<RootState, RootState> & RootGettersTypes = {
  getVersion: (state: RootState): string => {
    return state.version;
  },
};
