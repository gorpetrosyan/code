import { MutationTree } from "vuex";
import { MutationTypes } from "./mutationTypes";
import { RootMutationsTypes, RootState } from "./../../interfaces";

export const mutations: MutationTree<RootState> & RootMutationsTypes = {
  [MutationTypes.UPDATE_VERSION](state: RootState, payload: string) {
    state.version = payload;
  },
};
