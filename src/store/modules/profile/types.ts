import {
  ProfileStateTypes,
  ProfileMutationsTypes,
  ProfileGettersTypes,
  ProfileActionsTypes,
} from "@/store/interfacesR/profileInterface";
import { Store as VuexStore, CommitOptions, DispatchOptions } from "vuex";

export type ProfileStoreModuleTypes<S = ProfileStateTypes> = Omit<VuexStore<S>, "commit" | "getters" | "dispatch"> & {
  commit<K extends keyof ProfileMutationsTypes, P extends Parameters<ProfileMutationsTypes[K]>[1]>(
    key: K,
    payload?: P,
    options?: CommitOptions,
  ): ReturnType<ProfileMutationsTypes[K]>;
} & {
  getters: {
    [K in keyof ProfileGettersTypes]: ReturnType<ProfileGettersTypes[K]>;
  };
} & {
  dispatch<K extends keyof ProfileActionsTypes>(
    key: K,
    payload?: Parameters<ProfileActionsTypes[K]>[1],
    options?: DispatchOptions,
  ): ReturnType<ProfileActionsTypes[K]>;
};
