import { ProfileStateTypes } from "./../../interfacesR/profileInterface";

export const state: ProfileStateTypes = {
  profile: null,
  sports: null,
  clubStates: null,
};
