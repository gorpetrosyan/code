import { Module } from "vuex";
import { RootState } from "@/store/interfaces";
import { ProfileStateTypes } from "@/store/interfacesR/profileInterface";
import { getters } from "./getters";
import { actions } from "./actions";
import { mutations } from "./mutations";
import { state } from "./state";

// Module
const profile: Module<ProfileStateTypes, RootState> = {
  state,
  getters,
  mutations,
  actions,
};

export default profile;
