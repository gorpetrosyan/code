import { MutationTree } from "vuex";
import { MutationTypes } from "./mutationTypes";
import { ProfileMutationsTypes, ProfileStateTypes } from "./../../interfacesR/profileInterface";
export const mutations: MutationTree<ProfileStateTypes> & ProfileMutationsTypes = {
  [MutationTypes.SET_PROFILE](state: ProfileStateTypes, payload: Array<object> | null) {
    state.profile = payload;
  },
  [MutationTypes.RESET_PROFILE](state: ProfileStateTypes) {
    state.profile = null;
  },
  [MutationTypes.SET_SPORTS](state: ProfileStateTypes, payload: object | null) {
    state.sports = payload;
  },
  [MutationTypes.RESET_SPORTS](state: ProfileStateTypes) {
    state.sports = null;
  },
  [MutationTypes.SET_CLUB_STATES](state: ProfileStateTypes, payload: object | null) {
    state.clubStates = payload;
  },
  [MutationTypes.RESET_CLUB_STATES](state: ProfileStateTypes) {
    state.clubStates = null;
  },
};
