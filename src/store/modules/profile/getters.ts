import { GetterTree } from "vuex";
import { RootState } from "./../../interfaces";
import { ProfileGettersTypes, ProfileStateTypes } from "./../../interfacesR/profileInterface";

export const getters: GetterTree<ProfileStateTypes, RootState> & ProfileGettersTypes = {
  getProfileData: (state: ProfileStateTypes) => {
    return state.profile || null;
  },
  getSportData: (state: ProfileStateTypes) => {
    return state.sports || null;
  },
  getClubStateData: (state: ProfileStateTypes) => {
    return state.clubStates || null;
  },
  getProfileDataById: (state: ProfileStateTypes, id: number) => {
    if (typeof state.profile === "object") {
      // return state.profile.find((item: any) => {
      //   return item.id === id;
      // });
      console.log(id, "inside");
      return state.profile;
    }
    return null;
  },
};
