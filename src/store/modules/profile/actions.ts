import { ActionTree } from "vuex";
import { ActionTypes } from "./actionTypes";
import { MutationTypes } from "./mutationTypes";
import { RootState } from "@/store/interfaces";
import { ProfileActionsTypes, ProfileStateTypes } from "@/store/interfacesR/profileInterface";

export const actions: ActionTree<ProfileStateTypes, RootState> & ProfileActionsTypes = {
  [ActionTypes.GET_PROFILE]({ commit }, payload: Array<object> | null): void {
    commit(MutationTypes.SET_PROFILE, payload);
  },
  [ActionTypes.RESET_PROFILE]({ commit }): void {
    commit(MutationTypes.RESET_PROFILE);
  },
  [ActionTypes.GET_SPORTS]({ commit }, payload: object | null): void {
    commit(MutationTypes.SET_SPORTS, payload);
  },
  [ActionTypes.RESET_SPORTS]({ commit }): void {
    commit(MutationTypes.RESET_SPORTS);
  },
  [ActionTypes.GET_CLUB_STATES]({ commit }, payload: object | null): void {
    commit(MutationTypes.SET_CLUB_STATES, payload);
  },
  [ActionTypes.RESET_CLUB_STATES]({ commit }): void {
    commit(MutationTypes.RESET_CLUB_STATES);
  },
};
