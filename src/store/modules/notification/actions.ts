import { ActionTree } from "vuex";
import { ActionTypes } from "./actionTypes";
import { MutationTypes } from "./mutationTypes";
import { RootState } from "@/store/interfaces";
import { NotificationActionsTypes, NotificationStateTypes } from "@/store/interfacesR/notificationInterface";

export const actions: ActionTree<NotificationStateTypes, RootState> & NotificationActionsTypes = {
  [ActionTypes.GET_NOTIFICATIONS]({ commit }, payload: Array<object> | null): void {
    commit(MutationTypes.SET_NOTIFICATIONS, payload);
  },
  [ActionTypes.GET_UN_READ_NOTIFICATIONS]({ commit }, payload: number): void {
    commit(MutationTypes.SET_UN_READ_NOTIFICATIONS, payload);
  },
  [ActionTypes.GET_UN_SEEN_NOTIFICATIONS]({ commit }, payload: number): void {
    commit(MutationTypes.SET_UN_SEEN_NOTIFICATIONS, payload);
  },
  [ActionTypes.REMOVE_NOTIFICATION]({ commit }, payload: string): void {
    commit(MutationTypes.REMOVE_NOTIFICATION, payload);
  },
  [ActionTypes.SEEN_NOTIFICATION]({ commit }, payload: string): void {
    commit(MutationTypes.SET_SEEN_NOTIFICATION, payload);
  },
  [ActionTypes.READ_NOTIFICATION]({ commit }, payload: string): void {
    commit(MutationTypes.SET_READ_NOTIFICATION, payload);
  },
  [ActionTypes.GET_NOTIFICATION_PERMISSION]({ commit }, payload: boolean): void {
    commit(MutationTypes.SET_NOTIFICATION_PERMISSION, payload);
  },
  [ActionTypes.STATUS_NOTIFICATION]({ commit }, payload: string): void {
    // commit(MutationTypes., payload);
  },
};
