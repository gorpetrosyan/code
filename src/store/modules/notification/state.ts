import { NotificationStateTypes } from "./../../interfacesR/notificationInterface";
import { useLocalStorage } from "@/common/services/storage/localStorageService";
import { AllowPushNotifications } from "@/common/services/api/auth";

export const state: NotificationStateTypes = {
  notifications: null,
  unReadNotifications: 0,
  unSeenNotifications: 0,
  allowNotification: Boolean(useLocalStorage(AllowPushNotifications).value),
};
