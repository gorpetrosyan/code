import { Module } from "vuex";
import { RootState } from "@/store/interfaces";
import { NotificationStateTypes } from "@/store/interfacesR/notificationInterface";
import { getters } from "./getters";
import { actions } from "./actions";
import { mutations } from "./mutations";
import { state } from "./state";

// Module
const notification: Module<NotificationStateTypes, RootState> = {
  state,
  getters,
  mutations,
  actions,
};

export default notification;
