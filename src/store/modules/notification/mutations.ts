import {MutationTree} from "vuex";
import {MutationTypes} from "./mutationTypes";
import {NotificationMutationsTypes, NotificationStateTypes} from "./../../interfacesR/notificationInterface";

export const mutations: MutationTree<NotificationStateTypes> & NotificationMutationsTypes = {
  [MutationTypes.SET_NOTIFICATIONS](state: NotificationStateTypes, payload: Array<object> | null) {
    state.notifications = payload;
  },
  [MutationTypes.SET_UN_READ_NOTIFICATIONS](state: NotificationStateTypes, payload: number) {
    state.unReadNotifications = payload;
  },
  [MutationTypes.SET_NOTIFICATION_PERMISSION](state: NotificationStateTypes, payload: boolean) {
    state.allowNotification = payload;
  },
  [MutationTypes.SET_READ_NOTIFICATION](state: NotificationStateTypes, payload: string) {
    state.notifications?.find((notification: any) => {
      if (notification.id === payload) {
        notification.read = true;
      }
    });
  },
  [MutationTypes.SET_UN_SEEN_NOTIFICATIONS](state: NotificationStateTypes, payload: number) {
    state.unSeenNotifications = payload;
  },
  [MutationTypes.SET_SEEN_NOTIFICATION](state: NotificationStateTypes, payload: string) {
    state.notifications?.find((notification: any) => {
      if (notification.id === payload) {
        notification.seen = true;
      }
    });
  },
  [MutationTypes.REMOVE_NOTIFICATION](state: NotificationStateTypes, payload: string) {
    state.notifications?.find((notification: any, index: number) => {
      if (notification.id === payload) {
        state.notifications?.splice(index, 1)
      }
    });
  }
};
