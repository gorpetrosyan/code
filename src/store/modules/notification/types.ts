import {
  NotificationStateTypes,
  NotificationMutationsTypes,
  NotificationGettersTypes,
  NotificationActionsTypes,
} from "@/store/interfacesR/notificationInterface";
import { Store as VuexStore, CommitOptions, DispatchOptions } from "vuex";

export type NotificationStoreModuleTypes<S = NotificationStateTypes> = Omit<VuexStore<S>, "commit" | "getters" | "dispatch"> & {
  commit<K extends keyof NotificationMutationsTypes, P extends Parameters<NotificationMutationsTypes[K]>[1]>(
    key: K,
    payload?: P,
    options?: CommitOptions,
  ): ReturnType<NotificationMutationsTypes[K]>;
} & {
  getters: {
    [K in keyof NotificationGettersTypes]: ReturnType<NotificationGettersTypes[K]>;
  };
} & {
  dispatch<K extends keyof NotificationActionsTypes>(
    key: K,
    payload?: Parameters<NotificationActionsTypes[K]>[1],
    options?: DispatchOptions,
  ): ReturnType<NotificationActionsTypes[K]>;
};
