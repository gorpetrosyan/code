import { GetterTree } from "vuex";
import { RootState } from "./../../interfaces";
import { NotificationGettersTypes, NotificationStateTypes } from "./../../interfacesR/notificationInterface";

export const getters: GetterTree<NotificationStateTypes, RootState> & NotificationGettersTypes = {
  getNotificationData: (state: NotificationStateTypes, notificationId: string) => {
    return state.notifications?.find((notification: any) => notification.id === notificationId) || null;
  },
  getAllNotificationsData: (state: NotificationStateTypes) => {
    return state.notifications || null;
  },
  getUnReadNotifications: (state: NotificationStateTypes) => {
    return state.unReadNotifications;
  },
  getUnSeenNotifications: (state: NotificationStateTypes) => {
    return state.unSeenNotifications;
  },
  getNotificationAllowState: (state: NotificationStateTypes) => {
    return state.allowNotification;
  },
};
