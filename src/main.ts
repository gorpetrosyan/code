import { createApp } from "vue";
import { IonicVue } from "@ionic/vue";
import { store } from "./store";

import TheZportyApp from "./TheZportyApp";
import router from "./router";

/* Core CSS required for Ionic components to work properly */
import "@ionic/vue/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/vue/css/normalize.css";
import "@ionic/vue/css/structure.css";
import "@ionic/vue/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/vue/css/padding.css";
import "@ionic/vue/css/float-elements.css";
import "@ionic/vue/css/text-alignment.css";
import "@ionic/vue/css/text-transformation.css";
import "@ionic/vue/css/flex-utils.css";
import "@ionic/vue/css/display.css";

/* Theme variables */
import "./assets/styles/theme.scss";
import "./assets/styles/global.scss";

// Creat App
const app = createApp(TheZportyApp).use(IonicVue).use(router).use(store);

// ToDo Temp disable error logs in production
if (process.env.NODE_ENV == "production") {
  app.config.errorHandler = () => null;
  app.config.warnHandler = () => null;
}

// PWA
import { defineCustomElements } from "@ionic/pwa-elements/loader";

router.isReady().then(() => {
  // Initiate PWA
  defineCustomElements(window).then((r) => {
    console.log(r);
  });

  // Mount App
  app.mount("#app");
});
