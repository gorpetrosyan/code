import { Capacitor, Plugins } from "@capacitor/core";
const { Device } = Plugins;

const userDataInfo: { [key: string]: any } = {
  deviceId: "",
  deviceName: "",
  deviceType: "",
  os: "",
  platform: "",
  battery: "",
  language: "",
  geolocation: "",
  appVersion: "",
};

/**
 *  get user info
 */
async function getUserBrowserInfo(): Promise<object> {
  const navigator = window.navigator;
  return {
    device: "web",
    info: {
      platform: navigator.platform,
      clientInformation: window.clientInformation,
    },
  };
}

/**
 *  get user info
 */
async function getUserDeviceInfo(): Promise<any> {
  try {
    const device = Capacitor.getPlatform.name;
    const info: any = await Device.getInfo();
    info["batteryInfo"] = await Device.getBatteryInfo();
    info["languageInfo"] = await Device.getLanguageCode();
    return {
      device,
      info,
    };
  } catch (e) {
    console.log(e);
  }
}

/**
 * determine either is native app or not
 */
export function getDeviceDetails(): any {
  // console.log(Device.getInfo(), "device");
  return new Promise((resolve, reject) => {
    Device.getInfo()
      .then((device: any) => {
        resolve(device);
      })
      .then((e: any) => {
        reject(e);
      });
  });
  //return Capacitor.isNative ? getUserDeviceInfo() : getUserBrowserInfo();
}
