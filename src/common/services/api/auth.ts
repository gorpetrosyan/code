// import api from './api'
import { reactive, toRefs } from "vue";
import { useLocalStorage, clearFromLocalStorage } from "@/common/services/storage/localStorageService";

const AUTH_KEY = "zporty_token";
const USER_ID = "user_id";
export const AUTH_TOKEN = "access_token";
export const PROFILE_ID = "profile_id";
export const AllowPushNotifications = "allow_push";
export const PROFILE_TYPE = "profile_type";
export const WELCOME = "welcome";

interface User {
  id: string;
  email: string;
  dateOfBirth: Date;
  firstName: string;
  lastName: string;
  AUTH_TOKEN: string;
}

interface AuthState {
  authenticating: boolean;
  user?: User;
  error?: Error;
}

const state = reactive<AuthState>({
  authenticating: false,
  user: undefined,
  error: undefined,
});

// Read access token from local storage?
const token = useLocalStorage(AUTH_KEY).value;
if (!token) {
  console.log("no token");
}

export const useAuth = () => {
  const setUser = (payload: string, remember: boolean): void => {
    if (remember) {
      useLocalStorage(AUTH_KEY).value = payload;
    }
    state.error = undefined;
  };

  const setUserId = (userId: string, remember: boolean): void => {
    if (remember) {
      useLocalStorage(USER_ID).value = userId;
    }
    state.error = undefined;
  };

  const setProfileId = (profileId: string, remember: boolean): void => {
    if (remember) {
      useLocalStorage(PROFILE_ID).value = profileId;
    }
    state.error = undefined;
  };

  const setProfileType = (type: string, remember: boolean): void => {
    if (remember) {
      useLocalStorage(PROFILE_TYPE).value = type;
    }
    state.error = undefined;
  };

  const getUserId = (): string | null => {
    return useLocalStorage(USER_ID).value;
  };

  const getProfileId = (): string | null => {
    return useLocalStorage(PROFILE_ID).value;
  };

  const getProfileType = (): string | null => {
    return useLocalStorage(PROFILE_TYPE).value;
  };

  const getWelcome = (): string | null => {
    return useLocalStorage(WELCOME).value;
  };

  const setWelcome = (val: string) => {
    return (useLocalStorage(WELCOME).value = val);
  };

  const logout = (): Promise<void> => {
    clearFromLocalStorage(AUTH_KEY);
    return Promise.resolve((state.user = undefined));
  };

  const allowPushNotifications = (allow: boolean): void => {
    useLocalStorage(AllowPushNotifications).value = allow;
  };

  return {
    setWelcome,
    setUser,
    setUserId,
    setProfileId,
    setProfileType,
    getWelcome,
    getUserId,
    getProfileId,
    getProfileType,
    logout,
    allowPushNotifications,
    ...toRefs(state),
  };
};
