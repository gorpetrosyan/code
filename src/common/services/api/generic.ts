import { apiCore } from "@/common/services/api/data-api";
import Config from "@/config/environment";

const apiUrl = Config.API_URL;

export const genericApi = {
  getAgeGroups: async () => {
    return apiCore(apiUrl, "master/generic/ageGroup").get();
  },
  getDefaultAvatars: async () => {
    return apiCore(apiUrl, "master/generic/avatar").get();
  },
  getGenders: async () => {
    return apiCore(apiUrl, "master/generic/gender").get();
  },
  getPrivacyLabels: async () => {
    return apiCore(apiUrl, "master/generic/privacyLevel").get();
  },
  getRelationshipStatuses: async () => {
    return apiCore(apiUrl, "master/generic/relationshipStatus").get();
  },
  getSkillLevels: async () => {
    return apiCore(apiUrl, "master/generic/skillLevel").get();
  },
  getFeedbackList: async () => {
    return apiCore(apiUrl, "master/generic/feedBackType").get();
  },
};
