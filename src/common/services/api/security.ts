import { useApi } from "@/common/services/api/api";
import { useAuth } from "@/common/services/api/auth";
import { getDeviceDetails } from "@/common/services/device/info/deviceInfoService";
import { apiCore } from "@/common/services/api/data-api";
import Config from "@/config/environment";
import { hardReload } from "@/router/helpers";

const apiUrl = Config.AUTH_URL;

export const securityApi = {
  login: async (username: string, password: string, rememberMe = false) => {
    const { setUser, setUserId, setProfileId, setProfileType } = useAuth();
    const bodyFormData = new FormData();
    bodyFormData.append("username", username);
    bodyFormData.append("password", password);
    bodyFormData.append("grant_type", "password");
    bodyFormData.append("additional_info", JSON.stringify(await getDeviceDetails()));
    const data = await apiCore(
      apiUrl,
      "authentication/oauth/token",
      "application/x-www-form-urlencoded",
      "Basic " + btoa("zporty:jithinxavier"),
    )
      .post(bodyFormData)
      .catch((err) => {
        return err;
      });
    if (data.userId && data.access_token) {
      setUserId(data.userId, rememberMe);
      setUser(data.access_token, rememberMe);
      const me = await useApi("user/me").get();
      if (me.profiles.length) {
        setProfileId(me.profiles[0].id, rememberMe);
        setProfileType(me.profiles[0].type, rememberMe);
      }
      data.fresh = me.fresh;
    }
    return data;
  },
  me: async () => {
    if (useAuth().getUserId()) {
      return useApi("user/me").get();
    }
    return null;
  },
  register: async (email: string, firstName: string, lastName: string, dob: string, password: string) => {
    return await apiCore(apiUrl, "authentication/registrations/credentials").post({
      email,
      firstName,
      lastName,
      dob,
      password,
    });
  },
  requestOTP: async (email: string) => {
    return await apiCore(apiUrl, "authentication/security/password/forgot").put({ email });
  },
  changePassword: async (otp: string, email: string | string[], password: string) => {
    return await apiCore(apiUrl, "authentication/security/password/forgot/reset", "application/json").put({
      otp,
      email,
      newPassword: password,
    });
  },
  completeProfile: () => {
    const { setWelcome } = useAuth();
    setWelcome("0");
    hardReload("PlaygroundLanding");
  },
  verifyAccountOTP: async (otp: string, email: string | string[]) => {
    const { setUser, setUserId, setProfileId, setProfileType, setWelcome } = useAuth();
    return await apiCore(apiUrl, "authentication/registrations/credentials/verify", "application/json", "", {
      clientId: "zporty",
    })
      .post({ otp, email })
      .then(async (data) => {
        setUserId(data.userId, true);
        setUser(data.access_token, true);
        setWelcome("1");
        const me = await useApi("user/me").get();
        if (me.profiles.length) {
          setProfileId(me.profiles[0].id, true);
          setProfileType(me.profiles[0].type, true);
        }
      });
  },
  checkEmailAvailability: async (email: string) => {
    return await apiCore(apiUrl, "authentication/security/email-availability")
      .post({ email })
      .catch((e) => {
        return e;
      });
  },
};
