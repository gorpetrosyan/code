import { apiCore } from "@/common/services/api/data-api";
import Config from "@/config/environment";

const apiUrl = Config.API_URL;

export const eventApi = {
  getMyEventes: async (limit = 10, offset = 0) => {
    return apiCore(apiUrl, "events/user-event/myEvents?").get({ limit, offset });
  },
  /**
   * @param notificationID
   */
  getNotification: async (notificationID: string) => {
    return apiCore(apiUrl, `notification/${notificationID}`).get();
  },
  /**
   * @param limit
   * @param offset
   */
  searchNotification: async ( limit = 0, offset = 0) => {
    if (limit === 0) {
      return apiCore(apiUrl, 'notification').get()
    }
      return apiCore(apiUrl, 'notification').get({limit,offset})
  },
  /**
   * @param notificationId
   */
  deleteNotification: async (notificationId: string) => {
   return apiCore(apiUrl, `notification${notificationId}`).trash();
 },
  /**
   * @param notificationId
   */
  readNotification: async (notificationId: string) => {
    return apiCore(apiUrl, `notification/${notificationId}/read`).patch();
  },

  /**
   * @param notificationId
   */
  seenNotification: async (notificationId: string) => {
    return apiCore(apiUrl, `notification/${notificationId}/seen`).patch();
  },

  /**
   * @param notificationId
   */
  statusNotification: async (notificationId: string) => {
    return apiCore(apiUrl, `notification/${notificationId}/status`).patch();
  },

  /**
   * @param notificationIds
   */
  seeAllNotifications: async (notificationIds: Array<string>) => {
    return apiCore(apiUrl, 'notification/seeAll').patch({notificationIds});
  },

  /**
   *
   */
  unReadCountNotifications: async () => {
    return apiCore(apiUrl, 'notification/unReadCount').get();
  },

  /**
   *
   */
  unSeenCountNotification : async () => {
    return apiCore(apiUrl, 'notification/unSeenCount').get();
  }
};
