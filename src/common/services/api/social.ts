import { apiCore } from "@/common/services/api/data-api";
import Config from "@/config/environment";
import { PlaygroundFilter } from "@/common/models";

const apiUrl = Config.API_URL;

export const socialApi = {
  like: async (postId: number) => {
    return apiCore(apiUrl, "feed/like/post/" + postId).post({ emotionId: 1 });
  },
  unlike: async (postId: number) => {
    return apiCore(apiUrl, "feed/like/post/" + postId).trash();
  },
  createPost: async (data: any) => {
    return apiCore(apiUrl, "feed/post/").post(data);
  },
  getTrendingPosts: async (limit: number, offset: number) => {
    return apiCore(apiUrl, "feed/post/trending/week?limit=" + limit + "&offset=" + offset).get();
  },
  getRecommendedPosts: async (limit: number, offset: number) => {
    return apiCore(apiUrl, "feed/post/recommended?limit=" + limit + "&offset=" + offset).get();
  },
  getFriendsList: async (limit = 10, offset = 0) => {
    return apiCore(apiUrl, "user/app/friend/list?limit=" + limit + "&offset=" + offset).get();
  },
  getEvents: async (limit: number, offset: number) => {
    return apiCore(apiUrl, "events/search?limit=" + limit + "&offset=" + offset).get();
  },
  postUserEvent: async (data: any) => {
    return apiCore(apiUrl, "events/user-event/").post(data);
  },
  updateUserEvent: async (id: string, data: any) => {
    return apiCore(apiUrl, "events/user-event/" + id).put(data);
  },
  postGameEvent: async (data: any) => {
    return apiCore(apiUrl, "events/game/").post(data);
  },
  updateGameEvent: async (id: string, data: any) => {
    return apiCore(apiUrl, "events/game/" + id).put(data);
  },
  postTournamentEvent: async (data: any) => {
    return apiCore(apiUrl, "events/tournament/").post(data);
  },
  getGameEvent: async (id: string) => {
    return apiCore(apiUrl, "events/game/" + id).get();
  },
  getTournamentEvent: async (id: string) => {
    return apiCore(apiUrl, "events/tournament/" + id).get();
  },
  getUserEvent: async (id: string) => {
    return apiCore(apiUrl, "events/user-event/" + id).get();
  },
  updateTournamentEvent: async (id: string, data: any) => {
    return apiCore(apiUrl, "events/tournament/" + id).put(data);
  },
  getEventPlayers: async (eventId: number, limit = 10, offset = 0) => {
    return apiCore(apiUrl, "events/actions/" + eventId + "/player").get({ includeScore: true, limit, offset });
  },
  getEventMedia: async (id: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/media?limit=50&offset=0").get();
  },
  getTaggedPosts: async (id: number, limit = 10, offset = 0) => {
    return apiCore(apiUrl, "feed/post/taggedItem").get({ eventIds: id, limit, offset });
  },
  postJoinEvent: async (id: number) => {
    return apiCore(apiUrl, "events/actions/" + id + "/join").post();
  },
  postComment: async (id: string, data: any) => {
    return apiCore(apiUrl, "feed/post/" + id + "/comment").post(data);
  },
  getComments: async (id: string, limit = 10, offset = 0) => {
    return apiCore(apiUrl, "feed/post/" + id + "/search/comments?limit=" + limit + "&offset=" + offset).get();
  },
  deleteComments: async (referenceId: any) => {
    return apiCore(apiUrl, "feed/comment/" + referenceId).trash();
  },
  updateCommentUsingPUT: async (referenceId: any, commentsData: any) => {
    return apiCore(apiUrl, "feed/comment/" + referenceId).put(commentsData);
  },
  updateCommentReport: async (id: number, data: any) => {
    return apiCore(apiUrl, "feed/comment/" + id + "/report").put(data);
  },
  getPost: async (id: string) => {
    return apiCore(apiUrl, "feed/post/" + id).get();
  },
  getSocialPost: async (id: string, limit = 10, offset = 0) => {
    return apiCore(apiUrl, "feed/like/social_post/" + id + "/likes?limit=" + limit + "&offset=" + offset).get();
  },
  getReasons: async () => {
    return apiCore(apiUrl, "master/reporting/reason?limit=50&offset=0").get();
  },
  getConnectionsList: async (limit = 10, offset = 0) => {
    return apiCore(apiUrl, "user/connection/list").get({ limit, offset });
  },
  postBlockUser: async (data: any) => {
    return apiCore(apiUrl, "user/app/report/block").post(data);
  },
  getUserStats: async (id: number) => {
    return apiCore(apiUrl, "events/search/userStats?sportsId=" + id).get();
  },
  postUserVenue: async (data: any) => {
    return apiCore(apiUrl, "user/venues").post(data);
  },
  getEventRelations: async (eventId: number) => {
    return apiCore(apiUrl, "events/user-event/" + eventId + "/relation").get();
  },
  updateEventInvite: async (type: any, invitedId: any) => {
    return apiCore(apiUrl, "events/actions/invite/" + invitedId).put({ status: type });
  },
  updateEventRequest: async (type: any, requestId: any) => {
    return apiCore(apiUrl, "events/actions/join/" + requestId).put({ status: type });
  },
  leaveEvents: async (eventId: any, reportData: any) => {
    return apiCore(apiUrl, "events/actions/" + eventId + "/leave").post(reportData);
  },
  getChallengeDetails: async (id: number) => {
    return apiCore(apiUrl, "events/challenge/" + id).get();
  },
  updateChallengeStatusUsingPATCH: async (type: any, challenge: any) => {
    return apiCore(apiUrl, "events/challenge/" + challenge.challengeId + "/status").patch({
      status: type,
      shareToSocialFeed: true,
    });
  },
  deleteChallenge: async (challangeId: any) => {
    return apiCore(apiUrl, "events/challenge/" + challangeId).post();
  },
  updateScores: async (gameId: any, scoreRequestDTOList: any) => {
    return apiCore(apiUrl, "events/game/" + gameId + "/score/basic").post(scoreRequestDTOList);
  },
  manualUpdateGameResultsUsingPOST: async (gameId: any, manualupdates: any) => {
    return apiCore(apiUrl, "events/game/" + gameId + "/updateWinner").post(manualupdates);
  },
  getElasticStakeholders: async (keyword: string, limit = 10, offset = 0) => {
    let url = "user/search/elastic/stakeholders/all?limit=" + limit + "&offset=" + offset;
    if (keyword != "") {
      url = url + "&query=" + keyword;
    }
    return apiCore(apiUrl, url).get();
  },
  getElasticStakeholdersClubAndTeam: async (limit = 10, offset = 0) => {
    const url = "user/search/elastic/stakeholders/clubAndTeam?limit=" + limit + "&offset=" + offset;
    return apiCore(apiUrl, url).get();
  },
  getElasticStakeholdersUserProfile: async (limit = 10, offset = 0) => {
    const url = "user/search/elastic/stakeholders/userProfile?limit=" + limit + "&offset=" + offset;
    return apiCore(apiUrl, url).get();
  },
  getElasticStakeholdersFilter: async (
    limit: number,
    offset: number,
    keyword = "",
    category = "",
    gender = "",
    ageGroup = "",
    location = "",
  ) => {
    let url = "user/search/elastic/stakeholders/all?limit=" + limit + "&offset=" + offset;
    if (category) {
      url = url + "&category=" + category;
    }
    if (gender) {
      url = url + "&gender=" + gender;
    }
    if (ageGroup) {
      url = url + "&ageGroup=" + ageGroup;
    }
    if (location) {
      url = url + "&location=" + location;
    }
    if (keyword) {
      url = url + "&query=" + keyword;
    }
    return apiCore(apiUrl, url).get();
  },
  getElasticStakeholdersClubTeamFilter: async (
    limit: number,
    offset: number,
    keyword = "",
    category = "",
    gender = "",
    ageGroup = "",
    location = "",
  ) => {
    let url = "user/search/elastic/stakeholders/clubAndTeam?limit=" + limit + "&offset=" + offset;
    if (category) {
      url = url + "&category=" + category;
    }
    if (gender) {
      url = url + "&gender=" + gender;
    }
    if (ageGroup) {
      url = url + "&ageGroup=" + ageGroup;
    }
    if (location) {
      url = url + "&location=" + location;
    }
    if (keyword) {
      url = url + "&query=" + keyword;
    }
    return apiCore(apiUrl, url).get();
  },
  getElasticStakeholdersUserProfileFilter: async (
    limit: number,
    offset: number,
    keyword = "",
    category = "",
    gender = "",
    ageGroup = "",
    location = "",
  ) => {
    let url = "user/search/elastic/stakeholders/userProfile?limit=" + limit + "&offset=" + offset;
    if (category) {
      url = url + "&category=" + category;
    }
    if (gender) {
      url = url + "&gender=" + gender;
    }
    if (ageGroup) {
      url = url + "&ageGroup=" + ageGroup;
    }
    if (location) {
      url = url + "&location=" + location;
    }
    if (keyword) {
      url = url + "&query=" + keyword;
    }
    return apiCore(apiUrl, url).get();
  },
  getElasticStakeholdersCoachFilter: async (
    limit: number,
    offset: number,
    keyword: string,
    category: string,
    gender: string,
    ageGroup: string,
    location: string,
  ) => {
    let url = "user/search/elastic/stakeholders/coach?limit=" + limit + "&offset=" + offset;
    if (category != null) {
      url = url + "&category=" + category;
    }
    if (gender != null) {
      url = url + "&gender=" + gender;
    }
    if (ageGroup != null) {
      url = url + "&ageGroup=" + ageGroup;
    }
    if (location != null) {
      url = url + "&location=" + location;
    }
    if (keyword != "") {
      url = url + "&query=" + keyword;
    }
    return apiCore(apiUrl, url).get();
  },
  getEventsFilter: async (
    limit: number,
    offset: number,
    category: string,
    gender: string,
    ageGroup: string,
    keyword: string,
  ) => {
    let url = "events/search?limit=" + limit + "&offset=" + offset;
    if (category != null) {
      url = url + "&category=" + category;
    }
    if (gender != null) {
      url = url + "&gender=" + gender;
    }
    if (ageGroup != null) {
      url = url + "&ageGroup=" + ageGroup;
    }
    if (keyword != "") {
      url = url + "&query=" + keyword;
    }
    return apiCore(apiUrl, url).get();
  },
  getAppSettings: async () => {
    return apiCore(apiUrl, "user/settings/app").get();
  },
  updateAppSettings: async (data: any) => {
    return apiCore(apiUrl, "user/settings/app").put(data);
  },
  updateAppSettingsPatch: async (data: any) => {
    return apiCore(apiUrl, "user/settings/app").patch(data);
  },
  postUserFeedback: async (data: any) => {
    return apiCore(apiUrl, "user/feedback").post(data);
  },
  updateContactAccount: async (data: any) => {
    return apiCore(apiUrl, "user/settings/account/contact").put(data);
  },
  updateContactItem: async (data: any) => {
    return apiCore(apiUrl, "user/settings/account/contact").patch(data);
  },
  getUserNotifications: async (limit = 10, offset = 0) => {
    return apiCore(apiUrl, "user/notification/?limit=" + limit + "&offset=" + offset).get();
  },
  updateNotificationRead: async (notificationId: number) => {
    return apiCore(apiUrl, "user/notification/" + notificationId + "/read").patch();
  },
  updateAllNotificationsSeen: async (notificationIds = []) => {
    return apiCore(apiUrl, "user/notification/seeAll").patch(notificationIds);
  },
  getUnseenNotificationCount: async () => {
    return apiCore(apiUrl, "user/notification/unSeenCount").get();
  },
  getHighlightedPosts: async () => {
    return apiCore(apiUrl, "feed/post/highlightedPosts").get();
  },
  getHighlightedPostsConnections: async () => {
    return apiCore(apiUrl, "feed/post/highlightedPosts/connections?limit=50&offset=0").get();
  },
  getPlaygrounds: async (limit = 10, offset = 0) => {
    return apiCore(apiUrl, "feed/post/streamPlayGround?limit=" + limit + "&offset=" + offset).get();
  },
  getPlaygroundsFilter: async (filter: PlaygroundFilter, limit = 10, offset = 0) => {
    const newfilter = Object.assign({ limit, offset }, filter && filter.checked ? filter : {});
    return apiCore(apiUrl, "feed/post/streamPlayGround").get(newfilter);
  },
  updateAccountPassword: async (data: any) => {
    return apiCore(apiUrl, "user/settings/account/password").put(data);
  },
  updateUserName: async (data: any) => {
    return apiCore(apiUrl, "user/settings/account/username").put(data);
  },
  checkAvailabilityOfUserName: async (data: any) => {
    return apiCore(apiUrl, "user/settings/account/username-availability").post(data);
  },
  getFriendsListByQuery: async (limit: number, offset: number, query: string) => {
    return apiCore(apiUrl, "user/app/friend/list/?limit=" + limit + "&offset=" + offset + "&query=" + query).get();
  },
  postShareToProfile: async (id: any, data: any) => {
    return apiCore(apiUrl, "feed/post/" + id + "/shareToProfile/").post(data);
  },
  postEventChallenge: async (data: any) => {
    return apiCore(apiUrl, "events/challenge").post(data);
  },
  updateReport: async (id: number, data: any) => {
    return apiCore(apiUrl, "feed/post/" + id + "/report").put(data);
  },
  getRelationshipStatuses: async () => {
    return apiCore(apiUrl, "user/app/friend/requests/sent").get();
  },
  deletePost: async (id: string) => {
    return apiCore(apiUrl, "feed/post/" + id).trash();
  },
  updatePost: async (id: string, data: any) => {
    return apiCore(apiUrl, "feed/post/" + id).put(data);
  },
  deleteUserOtherSessions: async () => {
    return apiCore(apiUrl, "user/settings/account/sessions").trash();
  },
  deleteUserSelectedSession: async (deviceId: string) => {
    return apiCore(apiUrl, "user/settings/account/sessions/" + deviceId).trash();
  },
  getUserDeviceSessions: async () => {
    return apiCore(apiUrl, "user/settings/account/sessions").get();
  },
  postInvites: async (id: string, data: any) => {
    return apiCore(apiUrl, "events/actions/" + id + "/invites").post(data);
  },
  deleteInvites: async (id: string, data: any) => {
    return apiCore(apiUrl, "events/actions/" + id + "/invites").put(data);
  },
  getAdminIds: async (id: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/admin").get();
  },
  getInvites: async (id: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/invites").get();
  },
  deleteEvent: async (id: string) => {
    return apiCore(apiUrl, `events/user-event/${id}`).trash();
  },
};
