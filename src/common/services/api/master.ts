import { apiCore } from "@/common/services/api/data-api";
import Config from "@/config/environment";

const apiUrl = Config.API_URL;

export const masterApi = {
  getFaqList: async (limit: number, offset: number) => {
    return apiCore(apiUrl, "master/faq?limit=" + limit + "&offset=" + offset).get();
  },
  getPrivacyList: async (limit: number, offset: number) => {
    return apiCore(apiUrl, "master/privacy?limit=" + limit + "&offset=" + offset).get();
  },
  getTermsAndConditionsList: async (limit: number, offset: number) => {
    return apiCore(apiUrl, "master/terms?limit=" + limit + "&offset=" + offset).get();
  },
};
