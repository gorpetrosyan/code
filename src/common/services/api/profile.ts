import { apiCore } from "@/common/services/api/data-api";
import { useAuth, PROFILE_TYPE, PROFILE_ID } from "@/common/services/api/auth";
import Config from "@/config/environment";
import { hardReload } from "@/router/helpers";
import { useLocalStorage } from "@/common/services/storage/localStorageService";

const apiUrl = Config.API_URL;

export const profileApi = {
  me: async () => {
    return apiCore(apiUrl, "user/me").get();
  },
  getProfileId: (): number => {
    return parseInt(useLocalStorage(PROFILE_ID).value + "");
  },
  getProfileKind: (type: string): string => {
    const pkind = type || useLocalStorage(PROFILE_TYPE).value;
    if (pkind === "C") return "Coach";
    if (pkind === "V") return "Venue";
    if (pkind === "U") return "Player";
    if (pkind === "T") return "Club";
    if (pkind === "TM") return "Team";
    return "Player";
  },
  setProfile: (pf: any) => {
    useAuth().setProfileId(pf.id.toString(), true);
    useAuth().setProfileType(pf.type, true);
    hardReload();
  },
  getBasicInfo: async (profileId: number) => {
    return apiCore(apiUrl, `user/players/${profileId}/basic-info`).get();
  },
  getBasicClubInfo: async (profileId: number) => {
    return apiCore(apiUrl, `user/clubs/${profileId}/basic-info`).get();
  },
  getCommanProfileAvatar: async (profileId: number) => {
    return apiCore(apiUrl, `user/profiles/${profileId}/avatar`).get();
  },
  getBasicCoachInfo: async (profileId: number) => {
    return apiCore(apiUrl, `user/coaches/${profileId}/basic-info`).get();
  },
  getClubStats: async (profileId: number) => {
    return apiCore(apiUrl, `user/clubs/${profileId}/stats`).get();
  },
  getProfile: async (profileId: number) => {
    return apiCore(apiUrl, `user/players/${profileId}`).get();
  },
  getCoachProfile: async (profileId: number) => {
    return apiCore(apiUrl, `user/coaches/${profileId}`).get();
  },
  getCoachSports: async (profileId: number) => {
    return apiCore(apiUrl, `user/coaches/${profileId}/sports`).get();
  },
  getPhotos: async (profileId: number, limit = 0, offset = 0, includeDetails = true) => {
    if (limit === 0) {
      return apiCore(apiUrl, `feed/post/user/${profileId}/media`).get();
    } else {
      return apiCore(apiUrl, `feed/post/user/${profileId}/media`).get({ limit, offset, includeDetails });
    }
  },
  getSports: async (profileId: number) => {
    return apiCore(apiUrl, `user/players/${profileId}/sports`).get();
  },
  getFriends: async (profileId: number) => {
    return apiCore(apiUrl, `user/app/friend/${profileId}/list`).get();
  },
  getFollowersList: async (profileId: number, limit = 10, offset = 0) => {
    return apiCore(apiUrl, `user/app/follower/${profileId}/list`).get({ limit, offset });
  },
  getFollowingList: async (profileId: number, limit = 10, offset = 0) => {
    return apiCore(apiUrl, `user/app/follower/following/${profileId}/list`).get({ limit, offset });
  },
  getClubMembers: async (id: number, limit = 10, offset = 0) => {
    return apiCore(apiUrl, `user/clubs/${id}/members`).get({ limit, offset });
  },
  getPosts: async (profileId: number) => {
    return apiCore(apiUrl, `feed/post/user/${profileId}`).get({ profileId });
  },
  follow: async (profileId: number) => {
    return apiCore(apiUrl, `user/app/follower/${profileId}`).post({});
  },
  unfollow: async (profileId: number) => {
    return apiCore(apiUrl, `user/app/follower/${profileId}`).trash();
  },
  addFriend: async (profileId: number, greetingMessage: string) => {
    return apiCore(apiUrl, `user/app/friend/${profileId}`).post({ greetingMessage });
  },
  unfriend: async (profileId: number) => {
    return apiCore(apiUrl, `user/app/friend/${profileId}`).trash();
  },
  cancelFriendRequest: async (profileId: number) => {
    return apiCore(apiUrl, `user/app/friend/${profileId}/cancelFriendRequest`).trash();
  },
  getPlayerCategories: async () => {
    return apiCore(apiUrl, "master/category").get();
  },
  getPlayerSports: async () => {
    return apiCore(apiUrl, "master/sports").get();
  },
  createCoach: async (data: any) => {
    return apiCore(apiUrl, "user/coaches").post(data);
  },
  updateCoach: async (coachId: number, data: any) => {
    return apiCore(apiUrl, `user/coaches/${coachId}`).put(data);
  },
  updateClub: async (data: any, clubId: number) => {
    return apiCore(apiUrl, "user/clubs/" + clubId)
      .put(data)
      .then((result) => {
        profileApi.me();
        return result;
      });
  },
  addCoachSports: async (
    coachId: number,
    data: {
      skillGroup: number;
      sport: number;
    },
  ) => {
    return apiCore(apiUrl, `user/coaches/${coachId}/sports`).post(data);
  },
  removeCoachSports: async (
    coachId: number,
    sportId: number,
  ) => {
    return apiCore(apiUrl, `user/coaches/${coachId}/sports/${sportId}`).trash();
  },
  createTeam: async (clubId: number, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams`).post(data);
  },
  updateTeam: async (clubId: number, teamId: number, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}`).put(data);
  },
  updateUserFcmToken: async (token: object) => {
    return apiCore(apiUrl, `user/token/notification`).post(token);
  },
  removeAccount: async (data: any) => {
    return apiCore(apiUrl, "user/settings/account/delete").put(data);
  },
  createClub: async (data: any) => {
    return apiCore(apiUrl, "user/clubs")
      .post(data)
      .then((result) => {
        profileApi.me();
        return result;
      });
  },
  addSport: async (data: any) => {
    return apiCore(apiUrl, "user/players/sports").post(data);
  },
  removeSport: async (id: number) => {
    return apiCore(apiUrl, `user/players/sports/${id}`).trash();
  },
  updateSport: async (id: number, data: any) => {
    return apiCore(apiUrl, `user/players/sports/${id}`).patch(data);
  },
  getTeamList: async (clubId: number, limit = 10, offset = 0) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams`).get({ limit, offset });
  },
  getTeamInformation: async (teamId: number) => {
    return apiCore(apiUrl, `user/teams/${teamId}`).get();
  },
  getTeamMembers: async (teamId: number, limit = 10, offset = 0) => {
    return apiCore(apiUrl, `user/teams/${teamId}/members`).get({ limit, offset });
  },
  getClubSports: async (id: number) => {
    return apiCore(apiUrl, `user/clubs/${id}/sports`).get();
  },
  updatePlayers: async (data: any) => {
    return apiCore(apiUrl, "user/players").patch(data);
  },
  getUserSports: async () => {
    return apiCore(apiUrl, "user/players/sports").get();
  },
  getFriendRequests: async () => {
    return apiCore(apiUrl, "user/app/friend/requests").get();
  },
  getFriendRequestsSent: async () => {
    return apiCore(apiUrl, "user/app/friend/requests/sent").get();
  },
  handleFriendRequest: async (requestId: number, action: string) => {
    return apiCore(apiUrl, "user/app/friend/request").patch({ action: action, requestId: requestId });
  },
  createNotice: async (data: any) => {
    return apiCore(apiUrl, "feed/noticeboard").post(data);
  },
  getNoticeListData: async (id: any, limit: number, offset: number) => {
    return apiCore(apiUrl, `feed/noticeboard/profile/${id}`).get({ limit, offset });
  },
  getNoticeDetails: async (noticeId: any) => {
    return apiCore(apiUrl, `feed/noticeboard/${noticeId}`).get();
  },
  deleteNotice: async (noticeId: any) => {
    return apiCore(apiUrl, `feed/noticeboard/${noticeId}`).trash();
  },
  editNotice: async (noticeId: any, data: any) => {
    return apiCore(apiUrl, `feed/noticeboard/${noticeId}`).put(data);
  },
  updateClubAdmins: async (clubId: string, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/admins`).post(data);
  },
  removeClubAdmin: async (clubId: string, adminId: string) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/admins/${adminId}`).trash();
  },
  updateClubMembers: async (clubId: string, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/members`).post(data);
  },
  removeClubMember: async (clubId: string, memberId: string) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/members/${memberId}`).trash();
  },
  removeClubMembers: async (clubId: string, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/members`).put(data);
  },
  getTeamSports: async (clubId: string, teamId: string) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}/sports`).get();
  },
  getTeamStats: async (teamId: string) => {
    return apiCore(apiUrl, `user/teams/${teamId}/stats`).get();
  },
  updateTeamAdmins: async (clubId: string, teamId: string, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}/admins`).post(data);
  },
  removeTeamAdmin: async (clubId: string, teamId: string, adminId: string) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}/admins/${adminId}`).trash();
  },
  updateTeamMembers: async (clubId: string, teamId: string, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}/members`).post(data);
  },
  removeTeamMember: async (clubId: string, teamId: string, memberId: string) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}/members/${memberId}`).trash();
  },
  removeTeamMembers: async (clubId: string, teamId: string, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}/members`).put(data);
  },
  getClubMemberRequests: async (clubId: number) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/members/request/all`).get();
  },
  postClubMemberRequest: async (clubId: number, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/members/request`).post(data);
  },
  processClubMemberRequest: async (clubId: number, id: number, data: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/members/request/${id}/process`).post(data);
  },
  postClubRequest: async (clubId: number) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/requests`).post();
  },
  getClubInfo: async (clubId: number) => {
    return apiCore(apiUrl, `user/clubs/${clubId}`).get();
  },
  deleteTeamProfile: async (clubId: number, teamId: number) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}`).trash();
  },
  deleteClubProfile: async (clubId: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}`).trash();
  },
  deleteCoachProfile: async (coachId: any) => {
    return apiCore(apiUrl, `user/coaches/${coachId}`).trash();
  },
  leaveClubAdmin: async (clubId: any) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/admins/leave`).trash();
  },
  leaveTeamAdmin: async (clubId: number, teamId: number) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/teams/${teamId}/admins/leave`).trash();
  },
  getClubRequestStatus: async (clubId: number) => {
    return apiCore(apiUrl, `user/clubs/${clubId}/members/request/all`).get();
  },
};
