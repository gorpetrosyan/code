import axios, { AxiosRequestConfig } from "axios";
import { computed, ref } from "vue";
import { LoadingInstance } from "@/common/services/notifications/loader/loaderService";
import Config from "@/config/environment";
import { useLocalStorage } from "@/common/services/storage/localStorageService";

export const useApi = (endpoint: string, config?: any) => {
  const username = "zporty";
  const password = "jithinxavier";
  const Loader = new LoadingInstance();
  Loader.showLoader();
  const api = axios.create({
    baseURL: Config.AUTH_URL,
    headers: {
      Authorization: "Basic " + btoa(username + ":" + password),
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/x-www-form-urlencoded",
      "Access-Control-Allow-Headers": "Authorization",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
      Accept: "*/*",
    },
  });

  const apiJs = axios.create({
    baseURL: Config.AUTH_URL,
    headers: {
      Authorization:
        "Bearer " +
        btoa(
          "web:" +
            useLocalStorage("zporty_token").value +
            ":" +
            useLocalStorage("profile_type").value +
            ":" +
            useLocalStorage("profile_id").value,
        ),
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
      "Access-Control-Allow-Headers": "Authorization",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
      Accept: "*/*",
    },
  });

  const apiData = axios.create({
    baseURL: Config.API_URL,
    headers: {
      Authorization:
        "Bearer " +
        btoa(
          "web:" +
            useLocalStorage("zporty_token").value +
            ":" +
            useLocalStorage("profile_type").value +
            ":" +
            useLocalStorage("profile_id").value,
        ),
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
      "Access-Control-Allow-Headers": "Authorization",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
      Accept: "*/*",
    },
  });

  const fileUpload = axios.create({
    baseURL: Config.API_URL,
    headers: {
      Authorization:
        "Bearer " +
        btoa(
          "web:" +
            useLocalStorage("zporty_token").value +
            ":" +
            useLocalStorage("profile_type").value +
            ":" +
            useLocalStorage("profile_id").value,
        ),
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Headers": "Authorization",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
      Accept: "*/*",
    },
  });

  apiJs.interceptors.request.use(
    (config) => {
      config.headers.Authorization =
        "Bearer " +
        btoa(
          "web:" +
            useLocalStorage("zporty_token").value +
            ":" +
            useLocalStorage("profile_type").value +
            ":" +
            useLocalStorage("profile_id").value,
        );
      return config;
    },
    (error) => {
      return Promise.reject(error);
    },
  );

  apiData.interceptors.request.use(
    (config) => {
      config.headers.Authorization =
        "Bearer " +
        btoa(
          "web:" +
            useLocalStorage("zporty_token").value +
            ":" +
            useLocalStorage("profile_type").value +
            ":" +
            useLocalStorage("profile_id").value,
        );
      return config;
    },
    (error) => {
      return Promise.reject(error);
    },
  );

  fileUpload.interceptors.request.use(
    (config) => {
      config.headers.Authorization =
        "Bearer " +
        btoa(
          "web:" +
            useLocalStorage("zporty_token").value +
            ":" +
            useLocalStorage("profile_type").value +
            ":" +
            useLocalStorage("profile_id").value,
        );
      return config;
    },
    (error) => {
      return Promise.reject(error);
    },
  );

  const data = ref();
  const loading = ref(false);
  const error = ref();

  const postJs = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;

    return apiJs
      .post(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const putJs = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return apiJs
      .put(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const patchJs = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return apiJs
      .patch(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const putData = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return apiJs
      .put(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const patchData = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return apiData
      .patch(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const postApi = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return apiData
      .post(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const post = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return api
      .post(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const trash = () => {
    loading.value = true;
    error.value = undefined;
    return apiData
      .delete(endpoint)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;
        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const fileUploadJS = (payload?: Record<string, any>, config?: any) => {
    loading.value = true;
    error.value = undefined;

    return fileUpload
      .post(endpoint, payload, config)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const get = (query?: Record<string, any>, config?: AxiosRequestConfig) => {
    loading.value = true;
    error.value = undefined;

    let queryString = "";

    if (query) {
      queryString =
        "?" +
        Object.entries(query)
          .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
          .join("&");
    }

    return apiData
      .get(endpoint + queryString, config)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => {
        loading.value = false;
        Loader.hideLoader();
      });
  };

  const errorMessage = computed(() => {
    if (error.value) {
      return error.value.message;
    }
  });

  const errorDetails = computed(() => {
    if (error.value && error.value.response) {
      return error.value.response.data.message;
    }
  });

  const errorFields = computed(() => {
    if (error.value && Array.isArray(error.value.response.data.message)) {
      return (error.value.response.data.message as string[]).reduce((acc: Record<string, any>, msg: string) => {
        let [field] = msg.split(" ");

        // TODO: Maximal...
        if (field == "maximal") field = "dateOfBirth";

        if (!acc[field]) {
          acc[field] = [];
        }

        acc[field].push(msg);

        return acc;
      }, {});
    }
  });

  const computedClasses = (key: string) => {
    if (errorFields.value?.hasOwnProperty(key)) {
      return ["border-red-600", "bg-red-200", "text-red-900"];
    }
    return ["border-grey-600", "bg-white", "text-gray-900"];
  };

  return {
    loading,
    data,
    error,
    trash,
    get,
    post,
    postJs,
    putJs,
    patchJs,
    patchData,
    fileUploadJS,
    putData,
    postApi,
    errorMessage,
    errorDetails,
    errorFields,
    computedClasses,
  };
};
