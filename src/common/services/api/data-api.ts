import axios, { AxiosRequestConfig } from "axios";
import { computed, ref } from "vue";
import { useLocalStorage } from "@/common/services/storage/localStorageService";

export const apiCore = (
  url: string,
  endpoint: string,
  contentType = "application/json",
  defaultToken?: string,
  headers: any = {},
) => {
  const accessToken = useLocalStorage("zporty_token").value;
  const token = accessToken
    ? "Bearer " +
      btoa(
        "web:" +
          useLocalStorage("zporty_token").value +
          ":" +
          useLocalStorage("profile_type").value +
          ":" +
          useLocalStorage("profile_id").value,
      )
    : "Basic " + btoa("zporty:jithinxavier");
  const header = Object.assign(
    {},
    {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": contentType,
      "Access-Control-Allow-Headers": "Authorization",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE",
      Accept: "*/*",
    },
    headers,
  ) as any;
  if (token || defaultToken) {
    header["Authorization"] = token || defaultToken;
  }
  const apiAxios = axios.create({
    baseURL: url,
    headers: header,
  });
  const data = ref();
  const loading = ref(false);
  const error = ref();

  const post = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return apiAxios
      .post(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;
        // throw e;
        throw e.response.data;
      })
      .finally(() => (loading.value = false));
  };

  const trash = () => {
    loading.value = true;
    error.value = undefined;
    return apiAxios
      .delete(endpoint)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;
        throw e;
      })
      .finally(() => (loading.value = false));
  };

  const get = (query?: Record<string, any>, config?: AxiosRequestConfig) => {
    loading.value = true;
    error.value = undefined;

    let queryString = "";

    if (query) {
      queryString =
        "?" +
        Object.entries(query)
          .map(([key, value]) => (value + "" !== "" ? `${encodeURIComponent(key)}=${encodeURIComponent(value)}` : ""))
          .filter((x) => !!x)
          .join("&");
    }

    return apiAxios
      .get(endpoint + queryString, config)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;
        throw e;
      })
      .finally(() => (loading.value = false));
  };

  const put = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return apiAxios
      .put(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => (loading.value = false));
  };

  const patch = (payload?: Record<string, any>) => {
    loading.value = true;
    error.value = undefined;
    return apiAxios
      .patch(endpoint, payload)
      .then((res) => (data.value = res.data))
      .catch((e) => {
        error.value = e;

        throw e;
      })
      .finally(() => (loading.value = false));
  };

  const errorMessage = computed(() => {
    if (error.value) {
      return error.value.message;
    }
  });

  const errorDetails = computed(() => {
    if (error.value && error.value.response) {
      return error.value.response.data.message;
    }
  });

  const errorFields = computed(() => {
    if (error.value && Array.isArray(error.value.response.data.message)) {
      return (error.value.response.data.message as string[]).reduce((acc: Record<string, any>, msg: string) => {
        let [field] = msg.split(" ");

        // TODO: Maximal...
        if (field == "maximal") field = "dateOfBirth";

        if (!acc[field]) {
          acc[field] = [];
        }

        acc[field].push(msg);

        return acc;
      }, {});
    }
  });

  return {
    loading,
    data,
    error,
    trash,
    get,
    post,
    put,
    patch,
    errorMessage,
    errorDetails,
    errorFields,
  };
};
