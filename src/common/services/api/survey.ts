import { apiCore } from "@/common/services/api/data-api";
import Config from "@/config/environment";

const apiUrl = Config.API_URL;

export const surveyApi = {
  getCovidData: async (id: string, limit: number, offset: number) => {
    return apiCore(apiUrl, "events/actions/" + id + "/survey?limit=" + limit + "&offset=" + offset).get();
  },
  getEventSurvey: async (id: string, templateName: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/survey/" + templateName).get();
  },
  postCovidEvent: async (id: string, templateName: string, data: any) => {
    return apiCore(apiUrl, "events/actions/" + id + "/survey/" + templateName + "/feedback").post(data);
  },
  getTemplate: async (id: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/survey").get();
  },
  getMissingUsers: async (id: string, templateName: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/survey/" + templateName + "/missing").get();
  },
  getCovidUsers: async (id: string, templateName: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/survey/" + templateName + "/feedback").get();
  },
  getExportData: async (id: string, templateName: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/survey/" + templateName + "/feedback/export").get();
  },
  notifyUsers: async (id: string, templateName: string) => {
    return apiCore(apiUrl, "events/actions/" + id + "/survey/" + templateName + "/missing/notify").get();
  },
};
