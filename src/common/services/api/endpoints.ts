/**
 * store media files
 */
export const fileStoreEndPoint = "files/media";

/**
 * fcm push notification database url
 */
export const sendPushNotificationsWithDeviceDataUrl = "/token/notification/";

/**
 * chat token from rocket chat
 */
export const chatTokenFetchUrl = "/user/chat-token";
