import { apiCore } from "@/common/services/api/data-api";
import Config from "@/config/environment";
import { EncodeMediaOutput } from "../../models/EncodeMediaOutput";

const apiUrl = Config.API_URL;

export const mediaApi = {
  takeScreenshot: async (key: string, seconds: number): Promise<EncodeMediaOutput> => {
    return new Promise<EncodeMediaOutput>((resolve, reject) => {
      apiCore(apiUrl, "files/media/encode")
        .post({
          key,
          seconds,
          storageType: "private",
          profile: "thumbnail",
        })
        .then((result: EncodeMediaOutput) => {
          apiCore(apiUrl, "files/media")
            .get({ key: result.key, storageType: "private", fileType: "image", expiry: 3600 })
            .then((result) => {
              resolve(result as EncodeMediaOutput);
            });
        })
        .catch(reject);
    });
  },
  encodeVideo: async (key: string, profile: "original" | "mobile"): Promise<EncodeMediaOutput> => {
    return new Promise<EncodeMediaOutput>((resolve, reject) => {
      apiCore(apiUrl, "files/media/encode")
        .post({
          key,
          storageType: "private",
          profile,
        })
        .then((result: EncodeMediaOutput) => {
          apiCore(apiUrl, "files/media")
            .get({ key: result.key, storageType: "private", fileType: "image", expiry: 3600 })
            .then((result) => {
              resolve(result as EncodeMediaOutput);
            });
        })
        .catch(reject);
    });
  },
  parseKey: async (key: string): Promise<{ url: string }> => {
    return apiCore(apiUrl, "files/media").get({
      key,
      storageType: "private",
      fileType: "image",
    });
  },
};
