import { toastController } from "@ionic/vue";

/**
 * Toaster
 * @param type
 * @param text
 */
async function toasterService(type: string, text: string) {
  const toast = await toastController.create({
    message: text,
    duration: 2000,
    position: "top",
    color: type,
  });
  return toast.present();
}

const ToastTypes = {
  SUCCESS: "success",
  ERROR: "danger",
  INFO: "secondary",
};

async function successToaster(text: string) {
  return await toasterService(ToastTypes.SUCCESS, text);
}

async function errorToaster(text: string) {
  return await toasterService(ToastTypes.ERROR, text);
}
async function infoToaster(text: string) {
  return await toasterService(ToastTypes.INFO, text);
}
export { successToaster, errorToaster, infoToaster };
