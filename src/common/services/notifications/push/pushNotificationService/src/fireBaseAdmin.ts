import admin from "firebase-admin";
import { serviceAccount } from "@/config/properties/firebase/firebase-adminsdk";
import { firebaseConfig } from "@/config/properties/firebase/firebase-adminsdk-web";

/**
 * init firebase app/web
 */
export const fireBaseAdminInit = admin.initializeApp({
  credential: admin.credential.cert(JSON.stringify(serviceAccount)),
  databaseURL: firebaseConfig.databaseURL,
});
