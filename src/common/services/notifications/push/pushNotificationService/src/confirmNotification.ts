import { alertController } from "@ionic/vue";
import { pushNotifyOnLogin } from "@/common/services/notifications/push/pushNotificationService/pushNotificationService";

export async function confirmNotificationPermission() {
  const alert = await alertController.create({
    cssClass: "my-custom-class",
    header: "Turn on push notifications?",
    message:
      "Would you like to receive notifications on your device to help you keep up to date with stuff happening on Zporty?",
    buttons: [
      {
        text: "NO",
        role: "cancel",
        cssClass: "secondary",
        handler: (blah) => {
          pushNotifyOnLogin(false);
          alert.dismiss(true);
          return true;
        },
      },
      {
        text: "YES",
        handler: () => {
          pushNotifyOnLogin(true);
          alert.dismiss(false);
          return false;
        },
      },
    ],
  });
  return alert.present();
}
