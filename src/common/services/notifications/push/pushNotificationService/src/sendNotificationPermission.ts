import { useAuth } from "@/common/services/api/auth";
import { socialApi } from "@/common/services/api";
import { errorToaster, successToaster } from "@/common/services/notifications/toaster/toasterService";
import {AllActionTypes} from "@/store/actionTypes";
import {useStore} from "@/use/useStore";

export function sendPushNotificationPermission(obj: any) {
  const token = obj["token"];
  const store = useStore();
  const data: { [key: string]: any } = {
    token: token,
    allowNotifications: obj["allowNotifications"],
  };
  localStorage.setItem('allow_push', JSON.stringify(obj["allowNotifications"]));
  store.dispatch(AllActionTypes.GET_NOTIFICATION_PERMISSION, obj["allowNotifications"]);
  socialApi
    .updateAppSettingsPatch(data)
    .then((res: any) => {
      const { allowPushNotifications } = useAuth();
      allowPushNotifications(obj["allowNotifications"]);
      successToaster("Notification permission was allowed successfully");
    })
    .catch((e: any) => {
      errorToaster(e.message);
    });
}
