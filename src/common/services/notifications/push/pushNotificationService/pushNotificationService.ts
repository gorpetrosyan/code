import {Capacitor, Plugins, PushNotificationToken} from "@capacitor/core";

const { PushNotifications } = Plugins;
import { sendPushNotificationPermission } from "./src/sendNotificationPermission";
import { useAuth } from "@/common/services/api/auth";
import { getDeviceDetails } from "@/common/services/device/info/deviceInfoService";
import { eventApi, socialApi } from "@/common/services/api";
import { toastController} from '@ionic/vue';
import { useRouter } from "vue-router";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";
import { close, informationCircleOutline } from "ionicons/icons";
import moment from "moment";
import markAsRead from "@/common/services/notifications/push/notificationActions/markAsRead";
import storeFcmToken from "@/common/services/notifications/push/notificationActions/storeFcmToken";



/**
 * send push notification while login
 */
export async function pushNotifyOnLogin(allowPush: boolean): Promise<boolean> {
  // await PushNotifications.register();
  const isPushNotificationsAvailable = Capacitor.isPluginAvailable('PushNotifications');
  const router = useRouter();
  if (isPushNotificationsAvailable) {
    // await PushNotifications.addListener("registration", (token: PushNotificationToken) => {
      sendPushNotificationPermission({
        // token: token.value,
        allowNotifications: allowPush,
      });
    //   storeFcmToken(token.value);
    // });
    // await PushNotifications.addListener("registrationError", (error) => {
    //   errorToaster(JSON.stringify(error.message));
    // });
    //
    // await PushNotifications.addListener("pushNotificationReceived", async (notification: any) => {
    //   const modalT = await toastController.create({
    //     message: notification.text + '<br>' + moment(notification.notificationTime).format('HH:MM:SS'),
    //     color: 'light',
    //     buttons: [
    //       {
    //         side: 'start',
    //         icon: informationCircleOutline,
    //         handler: () => {
    //           router.push({
    //             name: notification.action.details.name,
    //             params: notification.action.details.params,
    //           })
    //           markAsRead(notification.id)
    //         }
    //       }, {
    //         icon: close,
    //         role: 'cancel',
    //       }
    //     ],
    //     position: 'top',
    //     header: notification.title,
    //     duration: 5000,
    //   });
    //   return modalT.present();
    // });
    // await PushNotifications.addListener("pushNotificationActionPerformed", async (notification: any) => {
    //   // Do something
    //   await router.push({
    //     name: notification.action.details.name,
    //     params: notification.action.details.params,
    //   })
    //   markAsRead(notification.id)
    // });
    // //
    // await PushNotifications.register();
  } else {
    const {allowPushNotifications} = useAuth();
    allowPushNotifications(allowPush);
  }

  return true;
}

/**
 *
 */
export async function deviceInfoData(): Promise<void> {
  getDeviceDetails().then((info: any) => {
    console.log(info, "here");
    socialApi
      .updateAppSettingsPatch(info)
      .then((res: any) => {
        console.log("Completed");
      })
      .catch((e: any) => {
        console.log(e.message);
      });
  });
}

