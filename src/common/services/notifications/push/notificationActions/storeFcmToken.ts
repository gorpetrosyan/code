import {profileApi} from "@/common/services/api";

export default function (token: string){
  profileApi.updateUserFcmToken({
    token: token
  }).then((res: any) => {
    console.log(res)
  })
}
