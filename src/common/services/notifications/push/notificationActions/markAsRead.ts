import { eventApi } from "@/common/services/api";
import { errorToaster } from "@/common/services/notifications/toaster/toasterService";

/**
 * @param notificationId
 */
export default function(notificationId: string){
  eventApi.readNotification(notificationId).then((res: any)=> {
    console.log(res);
  }).catch((e: any)=> {
    errorToaster(e.message)
  })
}
