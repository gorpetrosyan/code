import { loadingController } from "@ionic/vue";

export class LoadingInstance {
  private loadingController: any;

  constructor() {
    this.loadingController = loadingController;
  }
  // Show the loader for infinite time
  public showLoader(options = { message: "Please wait...", duration: 3500, cssClass: "", backdropDismiss: true }) {
    this.loadingController
      .create({
        message: options.message,
        duration: options.duration,
        backdropDismiss: options.backdropDismiss,
        cssClass: options.cssClass,
        translucent: true,
      })
      .then((res: any) => {
        res.present();
      });
  }

  // Hide the loader if already created otherwise return error
  public hideLoader() {
    this.loadingController
      .dismiss()
      .then((res: any) => {
        return res;
      })
      .catch((error: any) => {
        console.log("error", error);
      });
  }
}
