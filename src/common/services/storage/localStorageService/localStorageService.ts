import { ref, watch } from "vue";

function useLocalStorage(key: string) {
  const init = localStorage.getItem(key);
  //console.log("check profile", init);

  const variable = ref(init ? JSON.parse(init) : undefined);

  watch(
    () => variable.value,
    (to) => {
      localStorage.setItem(key, JSON.stringify(to));
    },
    { deep: true },
  );

  return variable;
}
function clearFromLocalStorage(key = "") {
  if (key) {
    localStorage.removeItem(key);
  } else {
    localStorage.clear();
  }
}

export { useLocalStorage, clearFromLocalStorage };
