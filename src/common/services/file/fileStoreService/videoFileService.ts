import { mediaApi } from "../../api/media";
import { VideoOutput } from "../../../models";

/**
 * encode video file as batch procss
 * @param key file key in storage
 */
export async function encodeVideo(key: string): Promise<VideoOutput> {
  const thumbnail = await mediaApi.takeScreenshot(key, 5);
  const original = await mediaApi.encodeVideo(key, "original");
  const mobile = await mediaApi.encodeVideo(key, "mobile");
  return {
    mobile,
    original,
    thumbnail,
  };
}
