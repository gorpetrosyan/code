import { useApi } from "@/common/services/api/api";
import { fileStoreEndPoint } from "@/common/services/api/endpoints";
import fileCompressor from "@/common/services/file/helpers/fileCompressor";
import { mediaTypes } from "@/config/properties/file-upload/file-upload-settings";
import { dataURItoBlob } from "@/common/services/file/helpers/dataURItoBlob";

/**
 * form data converter
 * @param payload
 */
function returnDataForm(payload: any): object {
  const bodyFormData = new FormData();
  bodyFormData.append("file", payload.file);
  bodyFormData.append("fileType", payload.fileType);
  bodyFormData.append("storageType", payload.storageType);
  return bodyFormData;
}

/**
 * store file to the server
 * @param file
 * @param fileType
 * @param storageType
 * @param config
 * @param endPoint
 * @param allowCompress
 */
export function storeFile(
  file: any,
  fileType: string,
  storageType: string,
  config: any,
  allowCompress = true,
  endPoint: string = fileStoreEndPoint,
) {
  const { fileUploadJS } = useApi(endPoint,config);
  if (fileType === mediaTypes.IMAGE && allowCompress) {
    return fileCompressor(dataURItoBlob(file)).then((res: any) => {
      const payload = {
        file: res,
        fileType: fileType,
        storageType: storageType,
      };
      return new Promise((resolve, reject) => {
        fileUploadJS(returnDataForm(payload))
          .then((res) => {
            resolve(res);
          })
          .catch((e) => {
            reject(e);
          });
      });
    });
  } else {
    const payload = {
      file: dataURItoBlob(file),
      fileType: fileType,
      storageType: storageType,
    };
    return new Promise((resolve, reject) => {
      fileUploadJS(returnDataForm(payload),config)
        .then((res) => {
          resolve(res);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }
}
