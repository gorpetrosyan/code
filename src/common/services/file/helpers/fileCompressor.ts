import Compressor from "compressorjs";
import { compressorQuality } from "@/config/properties/file-upload/file-upload-settings";
export default function (base64: any) {
  return new Promise((resolve, reject) => {
    new Compressor(base64, {
      quality: compressorQuality,
      success(result) {
        resolve(result);
      },
      error(err) {
        reject(err.message);
      },
    });
  });
}
