import { Crop } from "@ionic-native/crop";
import { photoQuality } from "@/config/properties/file-upload/file-upload-settings";

/**
 * @param url
 */
export async function cropImage(url: any): Promise<any> {
  return await Crop.crop(url, { quality: photoQuality })
    .then((imgUrl) => imgUrl)
    .catch((error) => error);
}
