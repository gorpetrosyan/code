export interface UploadResult {
  url: string;
  fileType: string;
  storageType: string;
  key: string;
  thumbnail: string;
  original: string;
  mobile: string;

  thumbnailKey: string;
  originalKey: string;
  mobileKey: string;
}
