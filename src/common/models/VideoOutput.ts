import { EncodeMediaOutput } from "./EncodeMediaOutput";

export interface VideoOutput {
  thumbnail: EncodeMediaOutput;
  mobile: EncodeMediaOutput;
  original: EncodeMediaOutput;
}
