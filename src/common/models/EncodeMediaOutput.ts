export interface EncodeMediaOutput {
  url: string;
  key: string;
}
