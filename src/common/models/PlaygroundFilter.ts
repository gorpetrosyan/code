export interface PlaygroundFilter {
  category: string;
  hashTags: string;
  location: string;
  query: string;
  sportsIds: string;
  checked: boolean;
}
