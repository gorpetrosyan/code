import { hardReload } from "@/router/helpers";
import { defineComponent } from "vue";
import { RouterLink } from "vue-router";

export default defineComponent({
  name: "RouterError",
  setup() {
    const styles = `  
          .landing {
            margin-left: 10%;
            width: 80%;
            text-align: center;
          }
          .not-found{
            margin-top: 10%;
            width: auto;
            max-height: 50vh;
          }
          h3{
            color: #333;
          }
    `;

    const getImage = require.context("./assets/", false, /\.png$/);
    const img = getImage("./bg.png");
    function reloadWindow() {
      hardReload("PlaygroundLanding");
    }

    return () => (
      <div>
        <style scoped>{styles}</style>
        <div class="landing">
          <img alt="404" src={img} class="not-found"></img>
          <h3>Oops! Got lost while trekking?</h3>
          <h4 onClick={reloadWindow}>
            <a>Get Home</a>
          </h4>
        </div>
      </div>
    );
  },
});
