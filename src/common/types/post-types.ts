enum postTypes {
  SOCIAL_FEED_POST = 1,
  EVENT = 2,
  GAME = 3,
  TOURNAMENT = 4,
  CHALLENGE = 5,
  ADMIN_CREATED_POST = 6,
  SHARED = 7,
}

export default postTypes;
