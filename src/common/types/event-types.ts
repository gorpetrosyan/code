enum eventTypes {
  GAME = "1",
  EVENT = "2",
  TOURNAMENT = "3",
  CHALLENGE = "4",
}

export default eventTypes;
