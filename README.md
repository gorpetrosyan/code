# End User Client App

End User Client App


## Getting Started

* [Download the installer](https://nodejs.org/) for Node LTS.
* Install the ionic CLI globally: `npm install -g ionic`
* Install the capacitor dependencies: `npm install @capacitor/core @capacitor/cli`
* Clone this repository.
* Run `npm install` from the project root.
* Run `ionic serve` in a terminal from the project root.
* Code Away! :tada:

_Note: See [How to Prevent Permissions Errors](https://docs.npmjs.com/getting-started/fixing-npm-permissions) if you are running into issues when trying to install packages globally._


## Building

* Run `ionic build` or `npm run ionic:build` from the project root.
* Run `npm run build-{{MODE}}` from the project root for custom build modes.  
  Example: `npm run build-dev`. Available modes: `dev`, `beta` & `production`.

## Coding

* Run `ionic serve` from the project root.
## Learn More

This application is written using Ionic Framework with Vue 3 and Capacitor JS.

* [Ionic with Vue](https://ionicframework.com/docs/vue/overview)
* [Vue 3 with Typescript](https://v3.vuejs.org/)
* [Capacitor](https://capacitorjs.com/)

