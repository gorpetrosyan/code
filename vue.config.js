module.exports = {
  chainWebpack: (config) => {
    config.resolve.extensions
      .add("*")
      .add(".vue")
      .add(".html")
      .add(".js")
      .add(".jsx")
      .add(".ts")
      .add(".tsx")
      .add(".css")
      .add(".scss")
      .add(".tsx")
      .add(".json");
    config.resolve.alias.set("@/", require("path").resolve(__dirname, "src/"));
  },
};
